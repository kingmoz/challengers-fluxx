# Fluxx #

This repo implemented the board game fluxx which can run in a browser. 

### What is this repository for? ###

For course CSCI4140's final project.

### How do I get set up? ###

0. Install [Node.js](https://github.com/nodejs/node)

1. npm install

2. node app.js

3. open http://localhost:8000/flow/

### How to play ###

1. Scan the qr code with a mobile device (Or you can copy the url and add /{player#} to url e.g. http://localhost:8000/flow/e27ce3a0-da3b-11e6-b8f8-63f95f27d771/1)

2. Press Start

3. Enjoy the game! (Rule [here](http://www.looneylabs.com/lit/rules/fluxx-rules))