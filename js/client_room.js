/*
*	Client Side socket function:
*		playerReady 		: 		Alert current Room that there is a player ready 
*		winningCondition	:		Reset the winningCondition in current Room 
*		rules				:		Reset the rules in current Room 
*		belongings			:		Reset the belongings in current Room 
*		discard				:		Reset the discard in current Room 
*		win					:		Return playerID that wins the game
*		playerHandsNum		:		Return all player's hand number
*		
********************************		
*   Function Needed : 
*		Endgame		
*/
//var roomID = window.location.pathname.substring(1);
var roomID = window.location.pathname.substring(1).split('/')[1];


 

var belongingsName = [];
var tmp_String = ["音樂", "月亮", "愛", "麵包", "眼睛", "派對", "烤麵面機", "和平", "腦", "太陽", "餅乾", "牛奶", "電視", "金錢", "夢", "時間", "巧克力", "睡眠", "火箭"];
for(var i = 20; i <= 38; i++){
	belongingsName[i] = tmp_String[i - 20];
}
//var socket = io( 'ws://' + window.location.hostname + ':3000/' ); 

var socket = io( 'ws://' + window.location.hostname + ':8000/' );
var discardsDeck = new Array();
var size_height;
var size_width;
function initPlayer(){
		var div = document.createElement("div");
		div.className = "row";
		div.id = "playerIcons";
		$('#game').append(div); 
		
	for (var i = 0; i < 6; i ++){
		var div = document.createElement("div");

		div.id = "user" + i;
		div.className = "col-xs-2"; 
		

		var img = document.createElement('img');
		img.src = "/user_icons/" + i + ".png";
		img.className = "col-xs-6";
		img.align = "middle"; 
		
		var ul = document.createElement('ul'); 
		ul.className = "belongingList"; 
		
		var span = document.createElement('span');
		span.innerHTML = "No. of Hands: 3";
		span.className = "col-xs-6 site__title little handsNum";
		
		div.appendChild(img);
		div.appendChild(span); 
		div.appendChild(ul);

		$('#playerIcons').append(div);
		$("#user" + i).hide();  
	}
}

function addPlayer(playerID){
	var qr_code = document.createElement('img');
	qr_code.src = "https://chart.googleapis.com/chart?cht=qr&chs=100x100&chl=" + encodeURIComponent(location.href + "/" + playerID);
	
	var header = document.createElement('p');
	header.innerHTML = "P" + (playerID + 1);
	
	var div = document.createElement('div');
	div.id = "player" + playerID;
	div.className = "col-xs-2";
	div.appendChild(header);
	div.appendChild(qr_code);
	
	$('#player').append(div);
}

function showingDeck(deckLength){
	//show deck
	var IMG_WIDTH = 190;
	var currentImg = 0;
	var maxImages = deckLength;
	var speed = 500;
	var imgs;
	var swipeOptions = {
		triggerOnTouchEnd: true,
		swipeStatus: swipeStatus,
		allowPageScroll: "vertical",
		threshold: 75
	};
	$(function () {
		imgs = $("#imgs");
		imgs.swipe(swipeOptions);
	});
	/**
	 * Catch each phase of the swipe.
	 * move : we drag the div
	 * cancel : we animate back to where we were
	 * end : we animate to the next image
	 */
	function swipeStatus(event, phase, direction, distance) {
		//If we are moving before swipe, and we are going L or R in X mode, or U or D in Y mode then drag.
		if (phase == "move" && (direction == "left" || direction == "right")) {
			var duration = 0;
			if (direction == "left") {
				scrollImages((IMG_WIDTH * currentImg) + distance, duration);
			} else if (direction == "right") {
				scrollImages((IMG_WIDTH * currentImg) - distance, duration);
			}
		} else if (phase == "cancel") {
			scrollImages(IMG_WIDTH * currentImg, speed);
		} else if (phase == "end") { 
			if (direction == "right") {
				previousImage();
			} else if (direction == "left") {
				nextImage();
			}
		}
	}  
	function previousImage() {
		currentImg = Math.max(currentImg - 1, 0);
		scrollImages(IMG_WIDTH * currentImg, speed);
	}
	function nextImage() {
		currentImg = Math.min(currentImg + 1, maxImages - 1);
		scrollImages(IMG_WIDTH * currentImg, speed);
	}
	/**
	 * Manually update the position of the imgs on drag
	 */
	function scrollImages(distance, duration) {
		imgs.css("transition-duration", (duration / 1000).toFixed(1) + "s");
		//inverse the number we set in the css
		var value = (distance < 0 ? "" : "-") + Math.abs(distance).toString();
		imgs.css("transform", "translate(" + value + "px,0)");
	}
	
}

//After page finish loading  
$(document).ready(function(){
	$('#game').hide();
	$('body').css('height',$(window).height()); 
	$('body').css('width',$(window).width());
	$('#game').css('height',$(window).height());
	$('#game').css('width',$(window).width()); 
	
	size_height = 3 * $(window).height() / 600;
	size_width = 3 * $(window).width() / 1366;
	
	
	
	if (size_height < size_width) {
		
		$('.mega2').css('font-size',  size_height + "rem");
	
	}
	else {
		$('.mega2').css('font-size',  size_width + "rem");
	} 
	
	window.onresize = function(event) {
		$('body').css('height',$(window).height()); 
		$('body').css('width',$(window).width());
		$('#game').css('height',$(window).height());
		$('#game').css('width',$(window).width()); 
		size_height = 3 * $(window).height() / 600;
		size_width = 3 * $(window).width() / 1366;
		
		if (size_height < size_width) {
			
			$('.mega2').css('font-size',  size_height + "rem");
		
		}
		else {
			$('.mega2').css('font-size',  size_width + "rem");
		}
	};
	
	
	for (var playerID = 0; playerID < 6; playerID ++){
		addPlayer(playerID);
	}
	initPlayer();
	//-1 to indicate register Computer
	socket.emit("register", roomID, -1, function(first){
		if (first == -1){
			$('#start').hide();
			socket.on('start', function(playNum){
				playNum = parseInt(playNum);
				console.log("Received playNum : " + playNum);
				for(var i = 0; i < playNum; i++){
					$("#user" + i).show();
				}
			$('#waitingForStart').hide();
			$('#game').show();
			});
		}
	});

	socket.on('playerReady', function(tmp_playerID){
		console.log("Recieved a player ready!");
		var ready = document.createElement('p');
		ready.innerHTML = "Player Ready";
		$('#player' + tmp_playerID).append(ready);
		$("#player" + tmp_playerID +  " img").attr('src', '/user_icons/' + tmp_playerID + '.png');
		$("#player" + tmp_playerID +  " img").width('100px');
	});
	socket.on('testmessage', function(data, callback){
		console.log('Socket (client-side): received message:', data);
		var responseData = { string1:'I like ', string2: 'bananas ', string3:' dude!' };
		//console.log('connection data:', evData); 
		callback(responseData); 
	});
 
	socket.on('winningCondition', function(conditions){
		console.log('Refresh ' + conditions.length + ' winningCondition : ' + conditions);
		$('#winningCondition').html("");
		$('#winningCondition').append("<h1 class='site__title mega2'>Winning Condition</h1>");
		if (conditions.length == 2){
			div = document.createElement("div");
			var img = document.createElement("img");
			img.className = "cards col-xs-6";
			img.src = "/cards/" + conditions[0] + ".jpg";
			div.appendChild(img);
			var img = document.createElement("img");
			img.className = "cards col-xs-6";
			img.src = "/cards/" + conditions[1] + ".jpg";
			div.appendChild(img);
			
			
			div.className = "win row";
			
			$('#winningCondition').append(div);
			
		
		}
		
		else {
			$('#winningCondition').append("<img class='cards' src = '/cards/" + conditions[0] + ".jpg'>");
		}
		
		
	});
	
	socket.on('rules', function(rules){
		console.log('Refresh rules : ' + rules);
		$('#rules').html("");
		$('.startGiveCard').html("");
		$('.startDrawCard').html("");
		$('#rules').append("<h1 class='site__title mega'>Rules</h1>");
		var len = 0;
		var maxheight;
		for(var i = 0; i < rules.length; i++){
			if (parseInt(rules[i]) == 72 || parseInt(rules[i]) == 73 ||parseInt(rules[i]) == 74 ||parseInt(rules[i]) == 75 ||parseInt(rules[i]) == 76 ||parseInt(rules[i]) == 77 ||parseInt(rules[i]) == 78 ||parseInt(rules[i]) == 79 ){
				
			}
			else {
				len++;
			}
		}
		if ( len == 1 ){
			maxheight = "70%";  
		
		}
		for(var i = 0; i < rules.length; i++){
			if (parseInt(rules[i]) == 72 || parseInt(rules[i]) == 73 ||parseInt(rules[i]) == 74 ||parseInt(rules[i]) == 75 ||parseInt(rules[i]) == 76 ||parseInt(rules[i]) == 77 ||parseInt(rules[i]) == 78 ||parseInt(rules[i]) == 79 ){
				if (parseInt(rules[i]) >= 72 && parseInt(rules[i]) <= 76){
					
					
					var img = document.createElement("img");
					img.className = "cards";
					img.src = "/cards/" + rules[i] + ".jpg";
					img.style = "max-height: 50%";
					$('.startGiveCard').append(img);
	
				}
				else {
					
					var img = document.createElement("img");
					img.className = "cards";
					img.src = "/cards/" + rules[i] + ".jpg";
					img.style = "max-height: 50%";
					$('.startDrawCard').append(img);
				}
			}
			else {
				$('#rules').append("<img class = 'cards' src = '/cards/" + rules[i] + ".jpg' style = 'max-height:" + maxheight + "'>"); 
			}
		}

	});
	
	socket.on('belongings', function(playerID, belongings){
		console.log('Refresh belongings : ' + belongings + ' of player : ' + playerID);
		
		$('#user' + playerID + ' .belongingList').html("");
		for(var i = 0; i < belongings.length; i++){
			$('#user' + playerID + ' .belongingList').append("<li class = 'belonging'>" + belongingsName[belongings[i]] + "</li>");
		}

		
	}); 
	
	socket.on('discard', function(discards){
		discardsDeck = new Array(); 
		console.log('Refresh discards : ' + discards);
/*		$('#discard').html("");
*/
		for(var i = 0; i < discards.length; i++){
/*			$('#discard').append("<img class = 'cards' src = '/cards/" + discards[i] + ".jpg'>");
*/
			discardsDeck[i] = discards[i];
		} 

	}); 
	
	socket.on('roomPlayCard', function (cardID){
		console.log("Received playCard Msg with ID : " + cardID);
		var div = document.createElement('div');
		div.className = "overlay";
		$('#game').append(div);
 
		//Add card in darken background  
		var div = document.createElement('div');
		div.id = "content";
		$('.overlay').append(div);
		var image = document.createElement('img');
		image.src = "/cards/" + cardID + ".jpg";
		image.id = "useCard";
		image.style.height = "300px";
		$('#content').append(image);
		 
		 
		$('#useCard').addClass('fadeInLeft animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
			//$(this).removeClass();
			console.log("[playCard]Inside Remove");
			$('.overlay').remove();
		});
			
		
	});

	socket.on('win', function(playerID){
		console.log("Received win situation");
		var div = document.createElement('div');
		div.className = "black_overlay";
		$('#game').append(div);
		//Add message area 
		var div = document.createElement('div');
		div.className = "messageArea";
		div.innerHTML = "Player " + playerID + " wins the game";
		$('.black_overlay').append(div);
		
		//Add images div in darken background
		var div = document.createElement('div');
		div.id = "winContent";
		$('.black_overlay').append(div);
		
		var image = document.createElement('img');
		image.src = "/user_icons/" + playerID + ".png";
		image.id = "winPlayer";
		image.style.height = "300px";
		$('#winContent').append(image);
		
	});
	
	socket.on('playerHandsNum', function(handsNum){
		console.log("Received playerHandsNum");
		for(var i = 0; i < handsNum.length; i ++){
			$("#user" + i + " .handsNum").html("No. of hands: " + handsNum[i]);
		}
	});
	
	socket.on('roomCurrentPlayer', function(playerID){
		for(var i = 0; i < 6; i++){
			$("#user" + i).removeClass("shake animated");
		}
		$("#user" + playerID).addClass("shake animated");
	});
	
	var start = document.createElement('button'); 
	start.innerHTML = "Start!";
	start.id = "start"; 
	start.className = "btn btn-success"; 
	start.onclick = function(){
		socket.emit('start', roomID, function(playNum){
			for(var i = 0; i < playNum; i++){

				$("#user" + i).show();

			}
		});
		$('#waitingForStart').hide();
		$('#game').show();
	};
	$('#button').append(start);
	
	$('#discard').click(function(){
		var msg = "Deck of discard"; 
		//Add darken background
		var div = document.createElement('div');
		div.className = "black_overlay";
		$('#game').append(div);
		//Add message area 
		var div = document.createElement('div');
		div.className = "messageArea";
		$('.black_overlay').append(div);
		
		var btn = document.createElement('button');
		btn.id = "cancel";
		btn.className = "btn btn-danger";
		btn.innerHTML = "Close window";
		$('.black_overlay').append(btn);
		
		//Add images div in darken background
		var div = document.createElement('div');
		div.id = "content2";
		$('.black_overlay').append(div);
		
		$('.messageArea').html(msg);
		
		var div = document.createElement('div');
		var width = discardsDeck.length * 190 + 20;
		div.id = "imgs";
		div.style.width = width + "px"; 
		$('#content2').append(div);
		
		for (var i = 0 ; i < discardsDeck.length ; i++){
			var image = document.createElement('img');
			image.src = "/cards/" + discardsDeck[i] + ".jpg";
			image.id = discardsDeck[i];
			image.style.height = "300px";
			if (i == 0) {
				image.style = "-webkit-transform: translate(5px, 0px)";
			
			
			}
			div.appendChild(image);
			$('#imgs').append(div);
		}		
		showingDeck(discardsDeck.length);
		
		$('#cancel').click(function(){
			$('.black_overlay').remove();
		
		});
		
	});
	
	
	
});