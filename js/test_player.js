/*	
	3: 抽3打2
	4: 扒手 show UI to select player first, then show that player's belongings
	5: 傳遞手牌 show choosing direction
	6: 以物易物 show choosing belonging twice
	7: 回收一張牌 
	8: 棄一張持有物
	10: 簡化規則
	11: 再來一次
	15: 棄一張新規則
	17: 抽2打2
	
add min max, need to test winning conditions, add message


*/
var roomID = window.location.pathname.substring(1).split('/')[0];
var playerID = parseInt(window.location.pathname.substring(1).split('/')[1]);
var deck_index = 0;
//var socket = io( 'ws://' + window.location.hostname + ':3000/' );
var socket = io( 'ws://' + window.location.hostname + ':8000/' );
var zoomed = false;
var baraja;

var savedZIndex = 0;
var savedRotation = 0;

//var deck = new Array();
var chosenCardArray = new Array();



function getRotationDegrees(obj) {
    var matrix = obj.css("-webkit-transform") ||
    obj.css("-moz-transform")    ||
    obj.css("-ms-transform")     ||
    obj.css("-o-transform")      ||
    obj.css("transform");
    if(matrix !== 'none') {
        var values = matrix.split('(')[1].split(')')[0].split(',');
        var a = values[0];
        var b = values[1];
        var angle = Math.round(Math.atan2(b, a) * (180/Math.PI));
    } 
	else { 
		var angle = 0; 
	}
    return angle;
}

(function() {
    var FX = {
        easing: {
            linear: function(progress) {
                return progress;
            },
            quadratic: function(progress) {
                return Math.pow(progress, 2);
            },
            swing: function(progress) {
                return 0.5 - Math.cos(progress * Math.PI) / 2;
            },
            circ: function(progress) {
                return 1 - Math.sin(Math.acos(progress));
            },
            back: function(progress, x) {
                return Math.pow(progress, 2) * ((x + 1) * progress - x);
            },
            bounce: function(progress) {
                for (var a = 0, b = 1, result; 1; a += b, b /= 2) {
                    if (progress >= (7 - 4 * a) / 11) {
                        return -Math.pow((11 - 6 * a - 11 * progress) / 4, 2) + Math.pow(b, 2);
                    }
                }
            },
            elastic: function(progress, x) {
                return Math.pow(2, 10 * (progress - 1)) * Math.cos(20 * Math.PI * x / 3 * progress);
            }
        },
        animate: function(options) {
            var start = new Date;
            var id = setInterval(function() {
                var timePassed = new Date - start;
                var progress = timePassed / options.duration;
                if (progress > 1) {
                    progress = 1;
                }
                options.progress = progress;
                var delta = options.delta(progress);
                options.step(delta);
                if (progress == 1) {
                    clearInterval(id);
                    options.complete();
                }
            }, options.delay || 10);
        },
        fadeOut: function(element, options) {
            var to = 1;
            this.animate({
                duration: options.duration,
                delta: function(progress) {
                    progress = this.progress;
                    return FX.easing.swing(progress);
                },
                complete: options.complete,
                step: function(delta) {
                    element.style.opacity = to - delta;
                }
            });
        },
        fadeIn: function(element, options) {
            var to = 0;
            this.animate({
                duration: options.duration,
                delta: function(progress) {
                    progress = this.progress;
                    return FX.easing.swing(progress);
                },
                complete: options.complete,
                step: function(delta) {
                    element.style.opacity = to + delta;
                }
            });
        }
    };
    window.FX = FX;
})();




//After page finish loading
$(document).ready(function(){
	$('body').bind('touchmove', function(e){e.preventDefault()});
	var $el 	= 	$( '#baraja-el' );
	baraja 	= 	$el.baraja();

	baraja.fan( {
		speed : 500,
		easing : 'ease-out',
		range : 60,
		direction : 'right',
		origin : { x : 50, y : 200 },
		center : true
	} );
	//Test Function
	$('#baraja-el').click( function(e){
		console.log('Clicked on ' + e.target.id);
	});				
	//Test Function
	$('#test').click(function(){
		baraja.drawCard($("<li><img class = 'img-zoom' id = '4' src = '../cards/4.jpg' height = '300px'></li>"));
	});
	
	//Show content according to orientation
	if(window.innerHeight > window.innerWidth){
		alert("Please use Landscape!");
		$('#horizontal').hide();
		$('#straight').show();
	}
	else {
		$('#straight').hide();
		$('#horizontal').show();
	
	}
	$(window).on("orientationchange",function(){
		if(window.orientation == 0)
		{
			alert("Please use Landscape!");
			$('#horizontal').hide();
			$('#straight').show();
		
		}
		else
		{
			$('#straight').hide();
			$('#horizontal').show();

		}
	});
	
	socket.emit("register", roomID, playerID);
	
	socket.on("start", function(){
		$('#wait').hide();
		$('#game').show();
	});
	
	socket.on('drawCard', function(card){
		deck_index = deck_index;
		console.log("Card : " + card);
		baraja.drawCard($("<li><img id = '" + card + "' src = '../cards/" + card + ".jpg' height = '300px'></li>"));
	});
	
	
	socket.on('selectDirection', function(data, callback){
		var _dir = "R"; 
		//Send the Selected direction to server
		callback(_dir);
	});
	socket.on('selectPlayer', function(data, callback){
		console.log('Socket (client-side): received message:', data);
		var responseData = { string1:'I like ', string2: 'bananas ', string3:' dude!' };
		//console.log('connection data:', evData);
		callback(1);
	});
	socket.on('selectMultiCard', function(message, cards, min, max, callback){
		console.log('Socket (client-side): received message:', cards);
		var responseData = { string1:'I like ', string2: 'bananas ', string3:' dude!' };
		SelectMultiCards(message, cards, min, max, callback);

	});
	$('#nextPlayer').click(function(){
		socket.emit("next_player",roomID,playerID)
	});
	socket.on('selectCard', function(message, cards , callback){
		console.log('Socket (client-side): received message:', cards);
		var responseData = { string1:'I like ', string2: 'bananas ', string3:' dude!' };
		SelectCards(message, cards, callback);

	});
	
	
	
	//enlarge the card when clicked
	$(".img-zoom").on("tap",function(){
		 if ( zoomed == false ) {
			savedZIndex = $(this).zIndex();
			$(this).addClass('transition');
			$(this).parent().zIndex('10000');
			$(this).parent().css('transform','rotate(0deg)');
			savedRotation = getRotationDegrees($(this).parent());
			
			zoomed = true;
		}
	});
	
	
	//minimize the enlarged card if the touched object is not enlarged
	$(document).on("tap",function(e){
		if ( zoomed == true && !$(event.target).hasClass('transition')){
			console.log("Taped");
			$('.transition').parent().css('transform','rotate('+savedRotation+'deg)')
			$('.transition').parent().zIndex(savedZIndex);
			$('.transition').removeClass('transition');
			zoomed = false;
		}
	});
	
	

	function showingDeck(deckLength){
		//show deck
		var IMG_WIDTH = 210;
		var currentImg = 0;
		var maxImages = deckLength;
		var speed = 500;
		var imgs;
		var swipeOptions = {
			triggerOnTouchEnd: true,
			swipeStatus: swipeStatus,
			allowPageScroll: "vertical",
			threshold: 75
		};
		
		imgs = $("#imgs");
		imgs.swipe(swipeOptions);
		
		/**
		 * Catch each phase of the swipe.
		 * move : we drag the div
		 * cancel : we animate back to where we were
		 * end : we animate to the next image
		 */
		function swipeStatus(event, phase, direction, distance) {
			//If we are moving before swipe, and we are going L or R in X mode, or U or D in Y mode then drag.
			if (phase == "move" && (direction == "left" || direction == "right")) {
				var duration = 0;
				if (direction == "left") {
					scrollImages((IMG_WIDTH * currentImg) + distance, duration);
				} else if (direction == "right") {
					scrollImages((IMG_WIDTH * currentImg) - distance, duration);
				}
			} else if (phase == "cancel") {
				scrollImages(IMG_WIDTH * currentImg, speed);

			} else if (phase == "end") {		

				if (direction == "right") {
					previousImage();
				} else if (direction == "left") {
					nextImage();
				}
			}
		}
		function previousImage() {
			console.log("In previousImage, currentImg : " + currentImg + "maxImages : " + maxImages);
			currentImg = Math.max(currentImg - 1, 0);
			scrollImages(IMG_WIDTH * currentImg, speed);
		}
		function nextImage() {
			console.log("In nextImage, currentImg : " + currentImg + "maxImages : " + maxImages);
			currentImg = Math.min(currentImg + 1, maxImages - 1);
			scrollImages(IMG_WIDTH * currentImg, speed);
		}
		/**
		 * Manually update the position of the imgs on drag
		 */
		function scrollImages(distance, duration) {
			imgs.css("transition-duration", (duration / 1000).toFixed(1) + "s");
			//inverse the number we set in the css
			var value = (distance < 0 ? "" : "-") + Math.abs(distance).toString();
			imgs.css("transform", "translate(" + value + "px,0)");
		}
		
	}
	
	function SelectCards(msg, deck, callback) {
//		deck = ["1", "5", "3", "80"];
		console.log("Calling selectCard Function");
		var desiredCard = -1;
		//Add darken background
		var div = document.createElement('div');
		div.className = "black_overlay";
		$('#game').append(div);
		//Add message area
		var div = document.createElement('div');
		div.className = "messageArea";
		$('.black_overlay').append(div);
		
		//Add images div in darken background
		var div = document.createElement('div');
		div.id = "content";
		$('.black_overlay').append(div);
		
		var div = document.createElement('div');
		var width = deck.length * 214;
		div.id = "imgs";
		div.style.width = width + "px";
		$('#content').append(div);
		for (var i=0;i < deck.length;i++){
			
			var div = document.createElement('div');
			var image = document.createElement('img');
			image.src = "/cards/" + deck[i] + ".jpg";
			image.id = deck[i];
			image.style.height = "300px";
			div.appendChild(image);
			$('#imgs').append(image);
			

		}		
		showingDeck(deck.length);
		
		
		
		var btn = document.createElement('button');
		btn.id = "abandon";
		btn.innerHTML = "Select";
		$('.black_overlay').append(btn);
		var btn = document.createElement('button');
		btn.id = "cancel";
		btn.innerHTML = "Cancel";
		$('.black_overlay').append(btn);
		var btn = document.createElement('button');
		btn.id = "finish";
		btn.innerHTML = "Finish";
		$('.black_overlay').append(btn);
		$('.messageArea').html(msg);
		
		$('#abandon').click(function(){
			
	
			var matrix = new WebKitCSSMatrix($('#imgs').css("transform"));
			var chosenCard = Math.abs(matrix.m41 / -210);//chosenCard : index
			var c = document.getElementById("imgs").childNodes;
			console.log("Chosen Card's ID: " + c[chosenCard].id);
			var chosenID = parseInt(c[chosenCard].id);
			
			if (c[chosenCard].className == "select"){//If the card is selected, user click select again
				$('.messageArea').html("This card has been selected!!");
				var msgArea = document.getElementsByClassName('messageArea');
				console.log(msgArea[0]);
				
				
				FX.fadeOut(msgArea[0], {
					duration: 3000,
					complete: function() {
						$('.messageArea').html(msg);
						msgArea[0].style.opacity = 1;
					}
				});
				
				
			
			}
			else {//the card has not been selected
				var slt = document.getElementsByClassName('select');
				console.log(slt);
				if (slt.length != 0){//user has already chosen another card
					$('.messageArea').html("Cannot select more than one card!!");
					var msgArea = document.getElementsByClassName('messageArea');
					console.log(msgArea[0]);
					
					
					FX.fadeOut(msgArea[0], {
						duration: 3000,
						complete: function() {
							$('.messageArea').html(msg);
							msgArea[0].style.opacity = 1;
						}
					});
				
				
				}
				else {//no card is selected before
					$('.messageArea').html(msg);
					desiredCard = chosenID;
					console.log("Card : " + desiredCard);
					c[chosenCard].className = "select";
				}
			
			}
	
		});
		
		
		
		$('#cancel').click(function(){
			var matrix = new WebKitCSSMatrix($('#imgs').css("transform"));
			var chosenCard = Math.abs(matrix.m41 / -210);//chosenCard : index
			var c = document.getElementById("imgs").childNodes;
			console.log("Chosen Card's ID: " + c[chosenCard].id);
			var chosenID = parseInt(c[chosenCard].id);
			
			if (c[chosenCard].className == "select"){//cancel the selected card
				$('.messageArea').html(msg);
				$(c[chosenCard]).removeClass('select');
				desiredCard = -1;
				console.log("[Cancel clicked]card : " + desiredCard);
			
			}
			else {
				$('.messageArea').html("This card is not selected!! Cancellation is not allowed!!");
				var msgArea = document.getElementsByClassName('messageArea');
				console.log(msgArea[0]);
				
				
				FX.fadeOut(msgArea[0], {
					duration: 3000,
					complete: function() {
						$('.messageArea').html(msg);
						msgArea[0].style.opacity = 1;
					}
				});
			
			}
		
		
		});
		
		$('#finish').click(function(){
		
			console.log("[Finish clicked]Card : " + desiredCard);
			if (desiredCard == -1){
				$('.messageArea').html("You didn't select a card!!Please choose one!!");
				var msgArea = document.getElementsByClassName('messageArea');
				console.log(msgArea[0]);
				
				
				FX.fadeOut(msgArea[0], {
					duration: 3000,
					complete: function() {
						$('.messageArea').html(msg);
						msgArea[0].style.opacity = 1;
					}
				});
			
			
			}
			else {
				callback(desiredCard);
				$('.black_overlay').remove();
			}
			
		});
	
	
	
	
	}
	
	



	function SelectMultiCards(msg, deck, min, max, callback) {
//		deck = ["1", "5", "3", "80"];
		var div = document.createElement('div');
		div.className = "black_overlay";
		$('#showDeckArea').append(div);
		
		
		var div = document.createElement('div');
		div.className = "messageArea";
		$('.black_overlay').append(div);
		
		
		var div = document.createElement('div');
		div.id = "content";
		$('.black_overlay').append(div);
		var div = document.createElement('div');
		var width = deck.length * 218;
		div.id = "imgs";
		div.style.width = width + "px";
		$('#content').append(div);
		for (var i=0;i < deck.length;i++){
			
			var div = document.createElement('div');
			var image = document.createElement('img');
			image.src = "/cards/" + deck[i] + ".jpg";
			image.id = deck[i];
			image.style.height = "300px";
			div.appendChild(image);
			$('#imgs').append(image);
			

		}		
		showingDeck(deck.length);
		
		
		
		var btn = document.createElement('button');
		btn.id = "abandon";
		btn.innerHTML = "Select";
		$('.black_overlay').append(btn);
		var btn = document.createElement('button');
		btn.id = "cancel";
		btn.innerHTML = "Cancel";
		$('.black_overlay').append(btn);
		var btn = document.createElement('button');
		btn.id = "finish";
		btn.innerHTML = "Finish";
		$('.black_overlay').append(btn);
		$('.messageArea').html(msg);
		
		$('#abandon').click(function(){
			
	
			var matrix = new WebKitCSSMatrix($('#imgs').css("transform"));
			var chosenCard = Math.abs(matrix.m41 / -210);//chosenCard : index
			var c = document.getElementById("imgs").childNodes;
			console.log("Chosen Card's ID: " + c[chosenCard].id);
			var chosenID = parseInt(c[chosenCard].id);
			
			if (c[chosenCard].className == "select"){
				$('.messageArea').html("This card has been select!!");
				var msgArea = document.getElementsByClassName('messageArea');
				console.log(msgArea[0]);
				
				
				FX.fadeOut(msgArea[0], {
					duration: 3000,
					complete: function() {
						$('.messageArea').html("");
						msgArea[0].style.opacity = 1;
					}
				});
				
				
			
			}
			else {
				$('.messageArea').html("");
				chosenCardArray.push(chosenID);
				console.log("Card Array: "+ chosenCardArray);
			
				
				c[chosenCard].className = "select";
				

	
			}
	
		});
		
		
		
		$('#cancel').click(function(){
			var matrix = new WebKitCSSMatrix($('#imgs').css("transform"));
			var chosenCard = Math.abs(matrix.m41 / -210);//chosenCard : index
			var c = document.getElementById("imgs").childNodes;
			console.log("Chosen Card's ID: " + c[chosenCard].id);
			var chosenID = parseInt(c[chosenCard].id);
			
			if (c[chosenCard].className == "select"){
				$('.messageArea').html("");
				$(c[chosenCard]).removeClass('select');
				chosenCardArray.splice(chosenCardArray.indexOf(chosenID), 1);
				console.log("[Cancel clicked]chosenCardArray : " + chosenCardArray);
			
			}
			else {
				$('.messageArea').html("No cards selected!! Cancellation is not allowed!!");
			
			}
		
		
		});
		
		$('#finish').click(function(){
			console.log("[Finish clicked]chosenCardArray : " + chosenCardArray);
			callback(chosenCardArray);
			$('.black_overlay').remove();
			
		});
	
	
	
	
	}
	
	
});