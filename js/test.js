
var roomID = window.location.pathname.substring(1);

//var socket = io( 'ws://' + window.location.hostname + ':3000/' );
var socket = io( 'ws://' + window.location.hostname + ':8000/' );

function addPlayer(playerID){
	var qr_code = document.createElement('img');
	qr_code.src = "https://chart.googleapis.com/chart?cht=qr&chs=100x100&chl=" + encodeURIComponent(location.href + "/" + playerID);
	
	var header = document.createElement('p');
	header.innerHTML = "P" + (playerID + 1);
	
	var div = document.createElement('div');
	div.id = "player" + playerID;
	div.appendChild(header);
	div.appendChild(qr_code);
	
	$('#player').append(div);
}

//After page finish loading
$(document).ready(function(){
	
	for (var playerID = 0; playerID < 2; playerID ++){
		addPlayer(playerID);
	}
	
	$("#add").click(function(){
		if (playerID < 6){
			addPlayer(playerID);
			playerID++;
		}
		if (playerID == 6) {
			$('#add').hide();
		}
	});
	
	socket.emit("register", roomID, -1);

	socket.on('playerReady', function(tmp_playerID){
		console.log("Recieved a player ready!");
		var ready = document.createElement('p');
		ready.innerHTML = "Player Ready";
		$('#player' + tmp_playerID).append(ready);
	});
	socket.on('testmessage', function(data, callback){
		console.log('Socket (client-side): received message:', data);
		var responseData = { string1:'I like ', string2: 'bananas ', string3:' dude!' };
		//console.log('connection data:', evData);
		callback(responseData);
	});
	socket.on('selectPlayer', function(data, callback){
		console.log('Socket (client-side): received message:', data, callback);
		var responseData = { string1:'I like ', string2: 'bananas ', string3:' dude!' };
		//console.log('connection data:', evData);
		callback(1);
	});

	var start = document.createElement('button');
	start.innerHTML = "Start!";
	start.id = "start"; 
	start.onclick = function(){
		socket.emit('TestFunc', roomID);
	};
	$('#canStart').append(start);
	
	socket.on('canStart', function(){
		
	});
});