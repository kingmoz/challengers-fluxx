/*
*	All need to add messages - pass in obejct instead 
*							   e.g {message:"XXX", cards : [1,2,3]}
*	Client Side socket function:
*		start 				: 		Game Start
*		drawCard			:		!important - draw oneCard at a time
*									Will Draw the cardID passed in
*		yourTurn			:		Alert current that it is their turn
*									Will pass in cards that they need to draw
*		playCard			:		Allow user to use card
*									Problems : need to add user notification
*		selectDirection		:		Let user to select direction (for passHands)
*		selectPlayer		:		Let user to select another player
*		selectCard			:		Let user to select card 
*		selectMultiCard({message, cards, min, max})		:		Let user to select cards
									Will pass in min and max
*		updateHands			:		Will remove all hands of current player
*									Then replace with new cards
*		
*/
//var roomID = window.location.pathname.substring(1).split('/')[0];
//Current RoomID : substring 2
var roomID = window.location.pathname.substring(1).split('/')[1];
var playerID = parseInt(window.location.pathname.substring(1).split('/')[2]);
var socket = io( 'ws://' + window.location.hostname + ':8000/' );
var zoomed = false;
var baraja;

var savedZIndex = 0;
var savedRotation = 0;
var chosenCardArray = new Array();

var dtap = false;
var mylatesttap;
function doubletap() {

   var now = new Date().getTime();
   var timesince = now - mylatesttap;
   if((timesince < 600) && (timesince > 0)){

//	 	double tap   
		dtap = true;

   }
   else{
		// too much time to be a doubletap
		dtap = false;
	}

   mylatesttap = new Date().getTime();
 }


	
function showingDeck(deckLength){
	//show deck
	var IMG_WIDTH = 126.6666;
	var currentImg = 0;
	var maxImages = deckLength;
	var speed = 500;
	var imgs;
	var swipeOptions = {
		triggerOnTouchEnd: true,
		swipeStatus: swipeStatus,
		allowPageScroll: "vertical",
		threshold: 75
	};
	$(function () {
		imgs = $("#imgs");
		imgs.swipe(swipeOptions);
	});
	/**
	 * Catch each phase of the swipe.
	 * move : we drag the div
	 * cancel : we animate back to where we were
	 * end : we animate to the next image
	 */
	function swipeStatus(event, phase, direction, distance) {
		//If we are moving before swipe, and we are going L or R in X mode, or U or D in Y mode then drag.
		if (phase == "move" && (direction == "left" || direction == "right")) {
			var duration = 0;
			if (direction == "left") {
				scrollImages((IMG_WIDTH * currentImg) + distance, duration);
			} else if (direction == "right") {
				scrollImages((IMG_WIDTH * currentImg) - distance, duration);
			}
		} else if (phase == "cancel") {
			scrollImages(IMG_WIDTH * currentImg, speed);
		} else if (phase == "end") { 
			if (direction == "right") {
				previousImage();
			} else if (direction == "left") {
				nextImage();
			}
		}
	}
	function previousImage() {
		currentImg = Math.max(currentImg - 1, 0);
		scrollImages(IMG_WIDTH * currentImg, speed);
	}
	function nextImage() {
		currentImg = Math.min(currentImg + 1, maxImages - 1);
		scrollImages(IMG_WIDTH * currentImg, speed);
	}
	/**
	 * Manually update the position of the imgs on drag
	 */
	function scrollImages(distance, duration) {
		imgs.css("transition-duration", (duration / 1000).toFixed(1) + "s");
		//inverse the number we set in the css
		var value = (distance < 0 ? "" : "-") + Math.abs(distance).toString();
		imgs.css("transform", "translate(" + value + "px,0)");
	}
	
}


function selectCard(msg, deck, callback) {
	console.log("Calling selectCard Function");
	var desiredCard = -1;
	//Add darken background
	var div = document.createElement('div');
	div.className = "black_overlay";
	$('#game').append(div);
	//Add message area 
	var div = document.createElement('div');
	div.className = "messageArea";
	$('.black_overlay').append(div);
	
	//Add images div in darken background
	var div = document.createElement('div');
	div.id = "content";
	$('.black_overlay').append(div);
	
	var div = document.createElement('div');
	var width = deck.length * 150.6666;
	div.id = "imgs";
	div.style.width = width + "px";
	$('#content').append(div);
	
	for (var i = 0 ; i < deck.length ; i++){
		var image = document.createElement('img');
		image.src = "/cards/" + deck[i] + ".jpg";
		image.id = deck[i];
		image.style.height = "200px"; 
		div.appendChild(image);
		$('#imgs').append(div);
	}		
	showingDeck(deck.length);
	
	var div = document.createElement('div');
	div.className = "btn-group";
	
	var btn = document.createElement('button');
	btn.id = "abandon";
	btn.className = "btn btn-primary";
	btn.innerHTML = "Select";
	div.appendChild(btn);
	var btn = document.createElement('button');
	btn.id = "cancel";
	btn.className = "btn btn-danger";
	btn.innerHTML = "Cancel";
	div.appendChild(btn);
	var btn = document.createElement('button');
	btn.id = "finish";
	btn.className = "btn btn-success";
	btn.innerHTML = "Finish";
	div.appendChild(btn);
	$('.black_overlay').append(div);
	$('.messageArea').html(msg);
	
	
	$('#abandon').click(function(){
		

		var matrix = new WebKitCSSMatrix($('#imgs').css("transform"));
		var chosenCard = Math.abs(matrix.m41 / -126.6666);//chosenCard : index
		var c = document.getElementById("imgs").childNodes;
		console.log("Chosen Card's ID: " + c[chosenCard].id);
		var chosenID = parseInt(c[chosenCard].id);
		
		if (c[chosenCard].className == "select"){//If the card is selected, user click select again
			$('.messageArea').html("This card has been selected!!");
			var msgArea = document.getElementsByClassName('messageArea');
			console.log(msgArea[0]); 
			
			
			FX.fadeOut(msgArea[0], {
				duration: 3000,
				complete: function() {
					$('.messageArea').html(msg);
					msgArea[0].style.opacity = 1;
				}
			});
			
			
		
		}
		else {//the card has not been selected
			var slt = document.getElementsByClassName('select');
			console.log(slt);
			if (slt.length != 0){//user has already chosen another card
				$('.messageArea').html("Cannot select more than one card!!");
				var msgArea = document.getElementsByClassName('messageArea');
				console.log(msgArea[0]);
				
				
				FX.fadeOut(msgArea[0], {
					duration: 3000,
					complete: function() {
						$('.messageArea').html(msg);
						msgArea[0].style.opacity = 1;
					}
				});
			
			
			}
			else {//no card is selected before
				$('.messageArea').html(msg);
				desiredCard = chosenID;
				console.log("Card : " + desiredCard);
				c[chosenCard].className = "select";
			}
		
		}

	});
	
	
	
	$('#cancel').click(function(){
		var matrix = new WebKitCSSMatrix($('#imgs').css("transform"));
		var chosenCard = Math.abs(matrix.m41 / -126.6666);//chosenCard : index
		var c = document.getElementById("imgs").childNodes;
		console.log("Chosen Card's ID: " + c[chosenCard].id);
		var chosenID = parseInt(c[chosenCard].id);
		
		if (c[chosenCard].className == "select"){//cancel the selected card
			$('.messageArea').html(msg);
			$(c[chosenCard]).removeClass('select');
			desiredCard = -1;
			console.log("[Cancel clicked]card : " + desiredCard);
		
		}
		else {
			$('.messageArea').html("This card is not selected!! Cancellation is not allowed!!");
			var msgArea = document.getElementsByClassName('messageArea');
			console.log(msgArea[0]); 
			
			
			FX.fadeOut(msgArea[0], {
				duration: 3000,
				complete: function() {
					$('.messageArea').html(msg);
					msgArea[0].style.opacity = 1;
				}
			});
		
		}
	
	
	});
	
	$('#finish').click(function(){
	
		console.log("[Finish clicked]Card : " + desiredCard);
		if (desiredCard == -1){
			$('.messageArea').html("You didn't select a card!!Please choose one!!");
			var msgArea = document.getElementsByClassName('messageArea');
			console.log(msgArea[0]);
			
			
			FX.fadeOut(msgArea[0], {
				duration: 3000,
				complete: function() {
					$('.messageArea').html(msg);
					msgArea[0].style.opacity = 1;
				}
			});
		
		
		}
		else {
			callback(desiredCard);
			$('.black_overlay').remove();
		}
		
	});





}
	
function selectMultiCard(msg, deck, min, max, callback) {
	console.log("Calling selectCard Function");
	var chosenCardArray = new Array();
	//Add darken background
	var div = document.createElement('div');
	div.className = "black_overlay";
	$('#game').append(div);
	//Add message area 
	var div = document.createElement('div');
	div.className = "messageArea";
	$('.black_overlay').append(div);
	
	//Add images div in darken background
	var div = document.createElement('div');
	div.id = "content";
	$('.black_overlay').append(div);
	
	var div = document.createElement('div');
	var width = deck.length * 150.6666;
	div.id = "imgs";
	div.style.width = width + "px";
	$('#content').append(div);
	
	for (var i = 0 ; i < deck.length ; i++){
		var image = document.createElement('img');
		image.src = "/cards/" + deck[i] + ".jpg";
		image.id = deck[i];
		image.style.height = "200px";
		if (i == 0) {
			image.style = "-webkit-transform: translate(5px, 0px)";
		
		
		}
		div.appendChild(image);
		$('#imgs').append(div);
	}		
	showingDeck(deck.length);
	
	var div = document.createElement('div');
	div.className = "btn-group";
	
	var btn = document.createElement('button');
	btn.id = "abandon";
	btn.className = "btn btn-primary";
	btn.innerHTML = "Select";
	div.appendChild(btn);
	var btn = document.createElement('button');
	btn.id = "cancel";
	btn.className = "btn btn-danger";
	btn.innerHTML = "Cancel";
	div.appendChild(btn);
	var btn = document.createElement('button');
	btn.id = "finish";
	btn.className = "btn btn-success";
	btn.innerHTML = "Finish";
	div.appendChild(btn);
	$('.black_overlay').append(div);
	$('.messageArea').html(msg);
	
	
	
	$('#abandon').click(function(){
		

		var matrix = new WebKitCSSMatrix($('#imgs').css("transform"));
		var chosenCard = Math.abs(matrix.m41 / -126.6666);//chosenCard : index
		var c = document.getElementById("imgs").childNodes;
		console.log("Chosen Card's ID: " + c[chosenCard].id);
		var chosenID = parseInt(c[chosenCard].id);
		
		if (c[chosenCard].className == "select"){//If the card is selected, user click select again
			$('.messageArea').html("This card has been selected!!");
			var msgArea = document.getElementsByClassName('messageArea');
			console.log(msgArea[0]);
			
			
			FX.fadeOut(msgArea[0], {
				duration: 3000,
				complete: function() {
					$('.messageArea').html(msg);
					msgArea[0].style.opacity = 1;
				}
			});
			
			
		
		}
		else {//the card has not been selected
			var slt = document.getElementsByClassName('select');
			console.log(slt);
			if (slt.length == max){//user has already chosen enough cards 
				$('.messageArea').html("Cannot select more cards!!");
				var msgArea = document.getElementsByClassName('messageArea');
				console.log(msgArea[0]);
				
				
				FX.fadeOut(msgArea[0], {
					duration: 3000,
					complete: function() {
						$('.messageArea').html(msg);
						msgArea[0].style.opacity = 1;
					}
				});
			
			
			}
			else {//not enough cards selected
				$('.messageArea').html(msg);
				chosenCardArray.push(chosenID);
				console.log("Card Array: "+ chosenCardArray);
				c[chosenCard].className = "select";
			}
		
		}

	});
	
	
	
	$('#cancel').click(function(){
		var matrix = new WebKitCSSMatrix($('#imgs').css("transform"));
		var chosenCard = Math.abs(matrix.m41 / -126.6666);//chosenCard : index
		var c = document.getElementById("imgs").childNodes;
		console.log("Chosen Card's ID: " + c[chosenCard].id);
		var chosenID = parseInt(c[chosenCard].id);
		
		if (c[chosenCard].className == "select"){//cancel the selected card
			$('.messageArea').html(msg);
			$(c[chosenCard]).removeClass('select');
			chosenCardArray.splice(chosenCardArray.indexOf(chosenID), 1);
			console.log("[Cancel clicked]chosenCardArray : " + chosenCardArray);
		
		}
		else {
			$('.messageArea').html("This card is not selected!! Cancellation is not allowed!!");
			var msgArea = document.getElementsByClassName('messageArea');
			console.log(msgArea[0]);
			
			
			FX.fadeOut(msgArea[0], {
				duration: 3000,
				complete: function() {
					$('.messageArea').html(msg);
					msgArea[0].style.opacity = 1;
				}
			});
		
		}
	
	
	});
	
	$('#finish').click(function(){
	
		console.log("[Finish clicked]chosenCardArray : " + chosenCardArray);
		if (chosenCardArray.length < min || chosenCardArray.length > max) {
			if (chosenCardArray.length < min) {
				$('.messageArea').html("You didn't choose enough cards!!");
				var msgArea = document.getElementsByClassName('messageArea');
				console.log(msgArea[0]);
				
				
				FX.fadeOut(msgArea[0], {
					duration: 3000,
					complete: function() {
						$('.messageArea').html(msg);
						msgArea[0].style.opacity = 1;
					}
				});
			}
			else {
				$('.messageArea').html("You have chosen too many cards!!");
				var msgArea = document.getElementsByClassName('messageArea');
				console.log(msgArea[0]);
				
				
				FX.fadeOut(msgArea[0], {
					duration: 3000,
					complete: function() {
						$('.messageArea').html(msg);
						msgArea[0].style.opacity = 1;
					}
				});
			
			}
		
		}
		else {
			callback(chosenCardArray);
			$('.black_overlay').remove();
			chosenCardArray = [];
		}
		
	});





}
	
function selectPlayer(playerIDs, msg, callback){
	console.log("Calling selectPlayer Function");
	
	//Add darken background
	var div = document.createElement('div');
	div.className = "black_overlay";
	$('#horizontal').append(div);
	//Add message area 
	var div = document.createElement('div');
	div.className = "messageArea";
	$('.black_overlay').append(div);
	//Add select player area
	var div = document.createElement('div');
	div.className = "container";
	$('.black_overlay').append(div);
	var div = document.createElement('div');
	div.className = "selectPlayer row";
	$('.container').append(div);
	$('.messageArea').html(msg);
	for (var i = 0 ; i < playerIDs.length; i++){
		var image = document.createElement('img');
		if (playerIDs[i] == 0){
			image.src = "/user_icons/0.png";
			image.id = "user0";
		}
		else if (playerIDs[i] == 1){
			image.src = "/user_icons/1.png";
			image.id = "user1";
		}
		else if (playerIDs[i] == 2){
			image.src = "/user_icons/2.png";
			image.id = "user2";
		}
		else if (playerIDs[i] == 3){
			image.src = "/user_icons/3.png";
			image.id = "user3";
		}
		else if (playerIDs[i] == 4){
			image.src = "/user_icons/4.png";
			image.id = "user4";
		}
		else {
			image.src = "/user_icons/5.png";
			image.id = "user5";
		}
		
		image.className = "user";
		div.appendChild(image);
		$('.selectPlayer').append(div);
	}	
	
	$('.user').click(function(e){
		
		
		
		console.log(e.target.src);
		var image = document.getElementsByClassName('user');
		if (e.target.id == "user0"){
			callback(0);
		}
		else if (e.target.id == "user1"){
			callback(1);
		}
		else if (e.target.id == "user2"){
			callback(2);
		}
		else if (e.target.id == "user3"){
			callback(3);
		}
		else if (e.target.id == "user4"){
			callback(4);
		}
		else {
			callback(5);
		}
		
		$('.black_overlay').remove();
		
		
		

	});



}

function selectDirection(msg, callback){
	console.log("Calling selectDirection Function");
	
	//Add darken background
	var div = document.createElement('div');
	div.className = "black_overlay";
	$('#horizontal').append(div);
	//Add message area 
	var div = document.createElement('div');
	div.className = "messageArea";
	$('.black_overlay').append(div);
	//Add select player area
	var div = document.createElement('div');
	div.className = "container";
	$('.black_overlay').append(div);
	var div = document.createElement('div');
	div.className = "selectDirection row";
	$('.container').append(div);
	$('.messageArea').html(msg);

	
	var image = document.createElement('img');
	image.src = "/user_icons/left.png";
	image.id = "left";
	image.className = "dir";
	div.appendChild(image);
	$('.selectDirection').append(div);
	
	var image = document.createElement('img');
	image.src = "/user_icons/right.png";
	image.id = "right";
	image.className = "dir";
	div.appendChild(image);
	$('.selectDirection').append(div);
	
	$('.dir').click(function(e){
		
		
	
		if (e.target.id == "left"){
			callback("L");
		}
		else {
			callback("R");
		}
		$('.black_overlay').remove();
		
	
	
	});
	
}

function getRotationDegrees(obj) {
    var matrix = obj.css("-webkit-transform") ||
    obj.css("-moz-transform")    ||
    obj.css("-ms-transform")     ||
    obj.css("-o-transform")      ||
    obj.css("transform");
    if(matrix !== 'none') {
        var values = matrix.split('(')[1].split(')')[0].split(',');
        var a = values[0];
        var b = values[1];
        var angle = Math.round(Math.atan2(b, a) * (180/Math.PI));
    } 
	else { 
		var angle = 0; 
	}
    return angle;
}

(function() {
    var FX = {
        easing: {
            linear: function(progress) {
                return progress;
            },
            quadratic: function(progress) {
                return Math.pow(progress, 2);
            },
            swing: function(progress) {
                return 0.5 - Math.cos(progress * Math.PI) / 2;
            },
            circ: function(progress) {
                return 1 - Math.sin(Math.acos(progress));
            },
            back: function(progress, x) {
                return Math.pow(progress, 2) * ((x + 1) * progress - x);
            },
            bounce: function(progress) {
                for (var a = 0, b = 1, result; 1; a += b, b /= 2) {
                    if (progress >= (7 - 4 * a) / 11) {
                        return -Math.pow((11 - 6 * a - 11 * progress) / 4, 2) + Math.pow(b, 2);
                    }
                }
            },
            elastic: function(progress, x) {
                return Math.pow(2, 10 * (progress - 1)) * Math.cos(20 * Math.PI * x / 3 * progress);
            }
        },
        animate: function(options) {
            var start = new Date;
            var id = setInterval(function() {
                var timePassed = new Date - start;
                var progress = timePassed / options.duration;
                if (progress > 1) {
                    progress = 1;
                }
                options.progress = progress;
                var delta = options.delta(progress);
                options.step(delta);
                if (progress == 1) {
                    clearInterval(id);
                    options.complete();
                }
            }, options.delay || 10);
        },
        fadeOut: function(element, options) {
            var to = 1;
            this.animate({
                duration: options.duration,
                delta: function(progress) {
                    progress = this.progress;
                    return FX.easing.swing(progress);
                },
                complete: options.complete,
                step: function(delta) {
                    element.style.opacity = to - delta;
                }
            });
        },
        fadeIn: function(element, options) {
            var to = 0;
            this.animate({
                duration: options.duration,
                delta: function(progress) {
                    progress = this.progress;
                    return FX.easing.swing(progress);
                },
                complete: options.complete,
                step: function(delta) {
                    element.style.opacity = to + delta;
                }
            });
        }
    };
    window.FX = FX;
})();



//After page finish loading 
$(document).ready(function(){
	
	
//	$('body').bind('touchmove', function(e){e.preventDefault()}); 
	//Variable for game Flow
	var handsNum = 0;
	var myTurn = 0;
	
	
	var cardsHeight = $(window).height() * 0.6;
	var cardsWidth = cardsHeight / 310 * 200;
	var rotateOrigin_x = $(window).width() * 0.1;
	var rotateOrigin_y = $(window).height() * 0.1;
	

	
	$('.baraja-container').css("width", cardsWidth + "px"); 
	$('.baraja-container').css("height", cardsHeight + "px");
	
	
	
	 
	
	//Show content according to orientation
	if(window.innerHeight > window.innerWidth){
		alert("Please use Landscape!");
		$('#horizontal').hide();
		$('#straight').show();
	}
	else {
		$('#straight').hide();
		$('#horizontal').show();
		
	}
	$(window).on("orientationchange",function(){
		if(window.orientation == 0){
			alert("Please use Landscape!");
			$('#horizontal').hide();
			$('#straight').show();
		
		}
		else{
			$('#straight').hide();
			$('#horizontal').show();
			cardsHeight = $(window).height() * 0.6;
			cardsWidth = cardsHeight / 310 * 200;
			rotateOrigin_x = $(window).width() * 0.1;
			rotateOrigin_y = $(window).height() * 0.1;
			
			selectCardsHeight = $(window).height() * 0.6;
			selectCardsWidth = selectCardsHeight / 300 * 190;
			ratioFontSize = selectCardsHeight / 300 * 30 + "px";
			
			
			$('.baraja-container').css("width", cardsWidth + "px"); 
			$('.baraja-container').css("height", cardsHeight + "px");
			
			$('#content').css("height", (selectCardsHeight + 20) + "px");

		}
	});
	
	
	var justPlayed = false;
	/*$('#baraja-el').click(function(e){
		

	});*/
	
	
	//minimize the enlarged card if the touched object is not enlarged
	
	
	$(document).on("tap", '#baraja-el', function(e){
		e.stopPropagation();
		e.stopImmediatePropagation();
		console.log("clicked document");
		if($(e.target).hasClass('transition')){
			if(myTurn == 1){
				zoomed = false;
				socket.emit('playCard', roomID, e.target.id);
				baraja.playCard($("#" + e.target.id).parent());
				handsNum--;
				myTurn = 0;
				$('.urTurnArea').html("");
				justPlayed = true;
			}
		}
		else if ( zoomed == true && !$(e.target).hasClass('transition')){
			console.log("Taped");
			$('.transition').parent().css('transform','rotate('+savedRotation+'deg)')
			$('.transition').parent().zIndex(savedZIndex);
			$('.transition').removeClass('transition');
			zoomed = false; 
		}
		else if ( zoomed == false && justPlayed == false) {
			console.log($("#" + e.target.id).parent());
			savedZIndex = $("#" + e.target.id).parent().zIndex();
			console.log(savedZIndex);
			$("#" + e.target.id).addClass('transition');
			$("#" + e.target.id).parent().zIndex('10000');
			$("#" + e.target.id).parent().css('transform','rotate(0deg)');
			savedRotation = getRotationDegrees($("#" + e.target.id).parent());
			console.log(savedRotation);
			zoomed = true;
		}
		setTimeout(function(){ justPlayed = false; }, 1000);
	});

	
	
	socket.emit("register", roomID, playerID);
	
	socket.on("start", function(){
		$('#wait').hide();
		$('#game').show();
		
	});
	  
	//Triger user to drawCard
	socket.on('drawCard', function(cardID){
		console.log("drawCard : " + cardID);
		if(zoomed == true){
			$('.transition').parent().css('transform','rotate('+savedRotation+'deg)')
			$('.transition').parent().zIndex(savedZIndex);
			$('.transition').removeClass('transition');
			zoomed = false; 
		}
		
		if(handsNum == 0){
			$('#baraja-el').append('<li><img class="img-zoom" id="'+ cardID +'" src="/cards/'+ cardID +'.jpg"></li>');
			var $el 	= 	$( '#baraja-el' );
			baraja 	= 	$el.baraja(); 

			baraja.fan( {
				speed : 500,
				easing : 'ease-out',
				range : 40,
				direction : 'right',
				origin : { x : rotateOrigin_x, y : rotateOrigin_y },
				center : true
			} );
		}
		else{
			baraja.drawCard($("<li><img class = 'img-zoom' id = '" + cardID + "' src = '/cards/" + cardID + ".jpg'></li>"));
		}
		handsNum ++;
	});
	
	//Let User to drawCard and use
	socket.on('yourTurn', function(cardIDs, cardRemaining){
		if(zoomed == true){
			$('.transition').parent().css('transform','rotate('+savedRotation+'deg)')
			$('.transition').parent().zIndex(savedZIndex);
			$('.transition').removeClass('transition');
			zoomed = false; 
		}
		zoomed = false; 
		for(var i = 0; i < cardIDs.length; i++){
			baraja.drawCard($("<li><img class = 'img-zoom' id = '" + cardIDs[i] + "' src = '/cards/" + cardIDs[i] + ".jpg'></li>"));
			handsNum ++;
		}
		//Allow user to playCard
		myTurn = 1;
		$('.urTurnArea').html("");
		var h = document.createElement("h1");
		var t = document.createTextNode("It's your turn. Play " + cardRemaining + " card");     // Create a text node
		h.appendChild(t); 
		$('.urTurnArea').append(h);
	});
	
	socket.on('playCard', function(cardRemaining){

		console.log("playCard");
		//Should Print something to let user print card

		myTurn = 1;
		$('.urTurnArea').html("");
		var h = document.createElement("h1");
		var t = document.createTextNode("It's your turn. Play " + cardRemaining + " card");     // Create a text node
		h.appendChild(t); 
		$('.urTurnArea').append(h);
	});
	
	socket.on('selectDirection', function(message, callback){
		var _dir = "R"; 
		selectDirection(message, callback);
		//Send the Selected direction to server 
//		callback(_dir);
	});
	socket.on('selectPlayer', function(message, playerIDs, callback){
		console.log('Player need to select players:', playerIDs);
		selectPlayer(playerIDs, message, callback);

	});
	socket.on('selectCard', function(message, cards, callback){
		console.log('Player need to select Card from :', cards);
		selectCard(message, cards, callback);
	});
	socket.on('selectMultiCard', function(message, cards, min, max, callback){
		console.log('Player need to select multiple cards from :', cards);
		selectMultiCard(message, cards, min, max, callback);

	}); 
	socket.on('updateHands', function(cards){
		console.log('UpdateHands With card number : ' + cards);
		$('#baraja-el').html("");
		handsNum = 0;
		if(cards.length > 0){
			$('#baraja-el').append('<li><img class="img-zoom" id="'+ cards[0] +'" src="/cards/'+ cards[0] +'.jpg"></li>');
			var $el 	= 	$( '#baraja-el' );
			baraja 	= 	$el.baraja();
			handsNum++;
			baraja.fan( {
				speed : 500,
				easing : 'ease-out',
				range : 40,
				direction : 'right',
				origin : { x : rotateOrigin_x, y : rotateOrigin_y },
				center : true
			} );
			for(var i = 1; i < cards.length; i++){
				baraja.drawCard($("<li><img class = 'img-zoom' id = '" + cards[i] + "' src = '/cards/" + cards[i] + ".jpg'></li>"));
				handsNum ++;
			}
		}
		
	});
	
	
	$('#nextPlayer').click(function(){
		socket.emit("next_player",roomID,playerID)
	});
	

	

	

	
});