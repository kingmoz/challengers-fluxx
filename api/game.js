var playerObj = require('./player');


var Game = function (playerNum) {
	//Number of player in this game
	this.playerNum = playerNum;
	//The number of cards needed to draw.
	this.drawCardNumber = 1;
	//The number of cards needed to play.
	this.playCardNumber = 1;
	//Store cards that were discarded 
	this.discard = [];
	//The ID of current player
	this.curPlayer = 0;
	this.nextPlayer = 1;
	this.winningCondition = [];
	this.rules = [];
	//Store the socket of the game room
	this.socket;

	this.handslimit = -1;
	this.belongingslimit = -1;

	//Check if player needs to play until X cards remain
	this.playthelastcard = -1;
	this.expand = 0;
	this.party = 0;
	
	//Number of Cards that the current player has played
	this.curPlayerplayed = 0;
	

	this.player = [];
	for(var i = 0; i < playerNum; i ++){
		var _player = new playerObj();
		this.player.push(_player);
	}
	
	//Init a shuffled deck
	var suf_array = [];
	for (i = 0 ; i <= 94 ; i++){
		suf_array[i] = i;
	}
	//Delete cards
	suf_array.splice(suf_array.indexOf(11),1);
	suf_array.splice(suf_array.indexOf(51),1);
	suf_array.splice(suf_array.indexOf(59),1);
	suf_array.splice(suf_array.indexOf(83),1);
	suf_array.splice(suf_array.indexOf(86),1);
	suf_array.splice(suf_array.indexOf(88),1);
	suf_array.splice(suf_array.indexOf(89),1);
	suf_array.splice(suf_array.indexOf(90),1);
	suf_array.splice(suf_array.indexOf(92),1);
	suf_array.splice(suf_array.indexOf(93),1);
	
	this.deck = this.shuffle(suf_array);

}

Game.prototype.addDiscard = function(cardID) {
	this.discard.push(cardID);
};

Game.prototype.getNextPlayer = function() {
	return this.playerNum;
};

//This function will need to check if there is 2 WinningCondition rules
Game.prototype.addWinningCondition = function(cardID) {
	this.winningCondition.push(cardID);
};

Game.prototype.addRules = function(cardID) {
	this.rules.push(cardID);
};

//Problem : Need to check remaining card in deck - King
Game.prototype.drawCard = function() {
	var tmp_card = this.deck[0];
	this.deck.splice(0,1);
	return tmp_card;
};

Game.prototype.shuffle =  function (array) {
  var currentIndex = array.length, temporaryValue, randomIndex ;

  // While there remain elements to shuffle...
  while (0 !== currentIndex) {

    // Pick a remaining element...
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex -= 1;

    // And swap it with the current element.
    temporaryValue = array[currentIndex];
    array[currentIndex] = array[randomIndex];
    array[randomIndex] = temporaryValue;
  }

  return array;
}


module.exports = Game;