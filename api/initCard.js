//Problems Here
//Discard belongings and hands have emit problem

/*
	Params : 
		game - the game object after init
	
	Socket.emit protocol:
		selectPlayer - Select Target of the card function
		selectCard(CardID Array) - Promot player to select From array
*/

var	q = require("q");

function checkWin(win){
		var deferred = q.defer();
		console.log("[initCardcheckWin] win : " + win);
		if(game.winningCondition.length > 1){
			console.log("[initCardcheckWin] For card 85");
			if (win[0] != -1 && typeof(win[0]) != "undefined" && win[0] != "" && win[0] >= 0 && win[0] <= 6){
				console.log("[initCardcheckWin]Player[" + win[0] + "] wins the game");
				game.socket.emit("win", win[0]);
			}
			else if(win[1] != -1 && typeof(win[1]) != "undefined" && win[1] != "" && win[1] >= 0 && win[1] <= 6){
				console.log("[initCardcheckWin]Player[" + win[1] + "] wins the game");
				game.socket.emit("win", win[1]);
			}
			else {
				deferred.resolve();
				return deferred.promise;
			}
		}
		else if(win != -1 && typeof(win) != "undefined" && win != "" && win >= 0 && win <= 6){
			console.log("[initCardcheckWin]Player[" + win + "] wins the game");
			game.socket.emit("win", win);
		}
		else{
			deferred.resolve();
			return deferred.promise;
		}
		
	}
	

exports.initCard = function(game){
	var card = [];
	
	
	/*
		Check if any player (except ignorePlayer) has belongings
		Return player that have belongings
	*/
	function getAllBelongings(ignorePlayer){
		var playerIDs = [];
		for(var i = 0; i < game.playerNum; i++){
			if (i === ignorePlayer){
				continue;
			}
			if (game.player[i].belongings.length > 0){
				playerIDs.push(i);
			}
			
		}
		return playerIDs;
	}
	
	/*
		Check if any player (except ignorePlayer) has hands
		Return players that have hands
	*/
	function getAllHands(ignorePlayer){
		var playerIDs = [];
		for(var i = 0; i < game.playerNum; i++){
			if (i === ignorePlayer){
				continue;
			}
			if (game.player[i].hands.length > 0){
				playerIDs.push(i);
			}
		}
		return playerIDs;
	}
	
	function removeduplicaterules(ruleID){
		console.log("duplicate rule ID: " + ruleID);
		console.log("indexof rule ID: " + game.rules.indexOf(ruleID))
		if(game.rules.indexOf(ruleID) != -1){
			game.rules.splice(game.rules.indexOf(ruleID),1);
			game.discard.push(ruleID);
			card[ruleID].remove();
			game.socket.emit('rules', game.rules);
			game.socket.emit('discard', game.discard);
		}
	}
	
	function executeCard(cardID, callback){
		cardID = parseInt(cardID);
		console.log(callback); 
		game.socket.emit("roomPlayCard", cardID);
		if(card[cardID].type == 1){			//Action
			game.discard.push(cardID);
			//Update Room
			game.socket.emit("discard", game.discard);
			card[cardID].func(callback);
			
			//Check if someone wins
			var calls = [];
			for(var i = 0; i < game.winningCondition.length; i ++){
				//card[game.winningCondition[i]].func(checkWin);
				calls.push(card[game.winningCondition[i]].func());
			}
			q.all(calls).then(checkWin); 
		}
		else if(card[cardID].type == 2){	//Belongings			
			//Update Room
			game.player[game.curPlayer].belongings.push(cardID);
			console.log("Adding belongings : " + game.player[game.curPlayer].belongings + " to main page");
			game.socket.emit('belongings', game.curPlayer,game.player[game.curPlayer].belongings);
			//Need to Check winningConditions
			//Check if someone wins
			var calls = [];
			for(var i = 0; i < game.winningCondition.length; i ++){
				calls.push(card[game.winningCondition[i]].func());
			}
			q.all(calls).then(callback);
		}		
		else if (card[cardID].type == 3){	//WinningCondition
			//If 雙重議程
			if(game.rules.indexOf(85) > -1){
				//If more are already two winningConditions - discard one
				if(game.winningCondition.length > 1){
					game.player[game.curPlayer].socket.emit("selectCard","[WinningCondition Limit]Discard a winningCondition", game.winningCondition, function(removeCardID){
						console.log("[85]Current player select discard winningCondition : " + removeCardID);	
						
						//Remove old card 
						game.winningCondition.splice(game.winningCondition.indexOf(removeCardID), 1);
						game.discard.push(removeCardID);
						game.socket.emit('discard', game.discard);

						
						//Add New card
						game.winningCondition.push(cardID);
						
						//Update Room
						console.log("Adding winningCondition : " + game.winningCondition + " to main page");
						game.socket.emit('winningCondition', game.winningCondition);
						
						card[cardID].func().then(callback);
					});
				}
				else{
					game.winningCondition.push(cardID);
					
					//Update Room
					console.log("Adding winningCondition : " + game.winningCondition + " to main page");
					game.socket.emit('winningCondition', game.winningCondition);
					
					card[cardID].func().then(callback);
				}
			}
			else{
				if(game.winningCondition.length > 0){
					game.discard.push(game.winningCondition[0]);
					game.socket.emit('discard', game.discard);
				}
				var tmp = [];
				tmp.push(cardID);
				game.winningCondition = tmp;
				
				//Update Room
				console.log("Adding winningCondition : " + game.winningCondition + " to main page");
				game.socket.emit('winningCondition', game.winningCondition);
				
				card[cardID].func().then(callback);
			}

			//Need to check if someone has win the game - Get return of func()
			//card[cardID].func(callback);
			
		}
		else if (card[cardID].type == 4){	//Rules
			game.rules.push(cardID);

			//Update Room
			console.log("Adding rules : " + game.rules + " to main page");
			game.socket.emit('rules', game.rules); 
			card[cardID].func().then(callback);
		}
	}
	
	for (var i = 0; i < 95; i++)
		card.push({});
	for (var i = 0; i < 20; i++)
		card[i].type = 1;
	for (var i = 20; i < 39; i++)
		card[i].type = 2;
	for (var i = 39; i < 69; i++)
		card[i].type = 3;
	for (var i = 69; i < 95; i++)
		card[i].type = 4;
	
	//Finished -- King
	//Added callback - King
	//Client side done - Woody
	//tested - ching
	card[0].func = function(callback){	//現搶現用
		var playerIDs = getAllHands(game.curPlayer);
		//select target
		if(playerIDs.length > 0){
			game.player[game.curPlayer].socket.emit('selectPlayer',"Please Select A Player!", playerIDs, function(playerID){
				playerID = parseInt(playerID);
				console.log('[0]User Callback called with data: ', playerID);
				if (game.player[playerID].hands.length == 0)	//no hands
					callback();
				else{
					//Random select a card from the target
					var randomNum = Math.floor((Math.random() * game.player[playerID].hands.length));
					console.log("[0] Player hands length : " + game.player[playerID].hands.length  + " Random number : " + randomNum);
					
					//discard it from the target
					console.log("[0] Player hands before splice : " + game.player[playerID].hands);
					var cardID = game.player[playerID].hands.splice(randomNum, 1);
					
					game.player[playerID].socket.emit('updateHands', game.player[playerID].hands); 
					console.log("[0] Player hands after splice : " + game.player[playerID].hands);
					
					//use the function of the card
					executeCard(cardID, callback);
				}
				
			});
		}
		else{				//No Player have hands
			callback();
		}
		
		
	};
	
	//Finished -- King
	//Added callback - King
	//Client side done - Woody
	//tested - ching
	card[1].func = function(callback){	//垃圾清運
	
		console.log("[1]Length before shuffle : " + game.deck.length);
		//將deck同discard pile重新洗過
		var newDeck = game.deck.concat(game.discard);
		game.deck = game.shuffle(newDeck);

		//discard this card
		game.discard = [1];
		game.socket.emit('discard', game.discard);
		console.log("[1]Length after shuffle : " + game.deck.length);
		callback();
	};
	
	//Problem : If player uses this card twice will fail
	//Added callback -- King
	card[2].func = function(callback){	//額外回合  //Assume infinity times
		//if用多過兩次就return, else continue
		console.log("[2]Current Next Player : " + game.nextPlayer);
		
		game.nextPlayer = game.curPlayer;

		console.log("[2]Processed Next Player : " + game.nextPlayer);
		callback();
	};

	//Finished - King
	//Added callback - King
	//tested - king
	card[3].func = function(callback){	//抽3打2
		var get = 3 + game.expand;
		var play = 2 + game.expand;
		var extraHands = [];

		
		//Check if there are enough card
		if(get > game.deck.length){
			var tmp_deck = game.discard.concat(game.deck);
			game.discard = [];
			//Shuffle card deck
			game.deck = game.shuffle(tmp_deck);
			console.log("[3]Deck after shuffle : " + game.deck);
		}
		
		//抽卡入extraHands
		for (var i = 0; i < get; i ++){
			extraHands.push(game.drawCard());
		}
		
		//Triger user to chooce card from cards
		//用卡
		game.player[game.curPlayer].socket.emit('selectCard', "Select a Card And Use!", extraHands, function(cardID1){
			cardID1 = parseInt(cardID1);
			console.log('[3]User select 1st card : ' + cardID1);
			extraHands.splice(extraHands.indexOf(cardID1), 1);
			executeCard(cardID1, function(){
				game.player[game.curPlayer].socket.emit('selectCard', "Select A Card And Use!", extraHands, function(cardID2){
					cardID2 = parseInt(cardID2);
					console.log('[3]User select 2nd card : ' + cardID2);
					extraHands.splice(extraHands.indexOf(cardID2), 1);
					if(play == 3){	//Need to play one more card
						executeCard(cardID2,
							game.player[game.curPlayer].socket.emit('selectCard', "Select A Card And Use!", extraHands, function(cardID3){
								cardID3 = parseInt(cardID3);
								console.log('[3]User select 3nd card : ' + cardID3);
								extraHands.splice(extraHands.indexOf(cardID3), 1);
								console.log('[3]User select 3rd card : ' + cardID3);
								//discard剩番果張
								
								game.discard.push(extraHands[0]);
	
								executeCard(cardID3, callback);
							})
						);
					}
					else{
						game.discard.push(extraHands[0]);
						executeCard(cardID2, callback);
					}
				})
			}
				
			);
		});

		
	};
	
	//Finished - King
	//Tested - ching
	//Added callback - King
	//Client side done - Woody
	card[4].func = function(callback){	//扒手
		var playerIDs = getAllBelongings(game.curPlayer);
		console.log('[4]Players: ' + playerIDs);
		if(playerIDs.length > 0){
			//select target

			console.log('[4] Socket players: ' + game.player);
			console.log('[4] Socket player: ' + game.player[game.curPlayer].hands);
			game.player[game.curPlayer].socket.emit('selectPlayer', "Choose a player to steal", playerIDs, function(playerID){
				playerID = parseInt(playerID);
				console.log('[4] User Callback called with playerID: ', playerID);
				
				if (game.player[playerID].belongings.length == 0){	//no belongings
					console.log("[4] Target Player has no belongings");
					callback();
				}
				else{
					//select the belongings from the target
					var message = "Select one belonging from Player " + playerID;
					game.player[game.curPlayer].socket.emit('selectCard', message, game.player[playerID].belongings, function(cardID){
						console.log('[4] User Callback called with cardID: ', cardID);
						
						//discard it from the target
						console.log("[4] Player belongings before splice : " + game.player[playerID].belongings);
						console.log("[4] Card index in player hands : " + game.player[playerID].belongings.indexOf(cardID));
						game.player[playerID].belongings.splice(game.player[playerID].belongings.indexOf(cardID), 1);
						game.socket.emit('belongings', playerID, game.player[playerID].belongings);
						console.log("[4] Player belongings after splice : " + game.player[playerID].belongings);
						
						//push the belongings into player
						console.log("[4] Player belongings before steal : " + game.player[game.curPlayer].belongings);
						game.player[game.curPlayer].belongings.push(cardID);
						game.socket.emit('belongings', game.curPlayer, game.player[game.curPlayer].belongings);
						console.log("[4] Player belongings after steal : " + game.player[game.curPlayer].belongings);
						
						var calls = [];
						for(var i = 0; i < game.winningCondition.length; i ++){
							calls.push(card[game.winningCondition[i]].func());
						}
						q.all(calls).then(callback); 

					});
					
				}
			
			});
		}
		else{
			console.log("[4] No Player has no belongings");
			callback();
		}
		
	};
	
	//Finished -- King
	//Not Yet tested - King
	//tested - ching
	//Added callback - King
	//Client side done - Woody
	//真糸tested ar fuck u
	card[5].func = function(callback){	//傳遞手牌
		var temp_hands = [];
		//Send request to player to let them select direction
		game.player[game.curPlayer].socket.emit('selectDirection', 'Please select a direction to pass the hands', function(dir){
			console.log('[5] User Callback called with data: ', dir);
			
			if(dir === "R"){			//if right : Assume right is previousPlayer
				temp_hands = game.player[0].hands;
				for (var i = 0; i < game.playerNum - 1; i++){
					game.player[i].hands = game.player[i+1].hands;
					game.player[i].socket.emit('updateHands', game.player[i].hands);
				}
				game.player[game.playerNum - 1].hands = temp_hands;
				game.player[game.playerNum - 1].socket.emit('updateHands', game.player[game.playerNum - 1].hands);
			}
			else if (dir === "L"){			//if left 
				temp_hands = game.player[game.playerNum - 1].hands;
				for (var i = game.playerNum - 1; i > 0; i--){
					game.player[i].hands = game.player[i-1].hands;
					game.player[i].socket.emit('updateHands', game.player[i].hands);
				}
				game.player[0].hands = temp_hands;
				game.player[0].socket.emit('updateHands', game.player[0].hands);
			}
			callback();
		});	
	};
	
	//Finished - King
	//Added callback - King
	//tested - ching
	//Client side done - Woody
	card[6].func = function(callback){	//以物易物
		var playerWithBelonging = [];
	
		//Check curPlayer's belongins
		if(game.player[game.curPlayer].belongings.length == 0){
			console.log("[6]Current user does not have belongings");
			callback();
			return;
		}
		for(var i = 0; i < game.playerNum; i++){
			if (i === game.curPlayer){
				continue;
			}
			console.log("[6] Player[" + i + "] Belonging : " + game.player[i].belongings);
			playerWithBelonging = playerWithBelonging.concat(game.player[i].belongings);
		}
		console.log("[6] Concated Array : " + playerWithBelonging);
		
		var playerIDs = getAllBelongings(game.curPlayer);
		//if對方無belongings,return
		if (playerIDs.length < 1){
			console.log("[6] No user have belongings");
			callback();
		}
		else{ 
			//select target
			game.player[game.curPlayer].socket.emit('selectPlayer', 'Please Select A Player!', playerIDs, function(playerID){
				playerID = parseInt(playerID);
				console.log('[6] User Callback called with playerID: ', playerID);
				//Select Belongings From Target player
				game.player[game.curPlayer].socket.emit('selectCard', "Select A Belonging From Player!", game.player[playerID].belongings, function(TargetID){
					console.log('[6] User Callback called with TargetCardID: ', TargetID);
					//Select belongings from self
					game.player[game.curPlayer].socket.emit('selectCard', "Select A Belonging For Exchange!", game.player[game.curPlayer].belongings, function(SelfID){
						console.log('[6] User Callback called with SelfCardID: ', SelfID);
						
						//Swap
						
						//Remove Card
						console.log('[6] Target Player belongings before Swap : ', game.player[playerID].belongings);
						console.log('[6] Self belongings before Swap : ', game.player[game.curPlayer].belongings);
						game.player[game.curPlayer].belongings.splice(game.player[game.curPlayer].belongings.indexOf(SelfID), 1);
						game.player[playerID].belongings.splice(game.player[playerID].belongings.indexOf(TargetID), 1);
						//Add Card
						
						game.player[game.curPlayer].belongings.push(TargetID);
						game.player[playerID].belongings.push(SelfID);
						game.socket.emit('belongings', game.curPlayer, game.player[game.curPlayer].belongings);
						game.socket.emit('belongings', playerID, game.player[playerID].belongings);
						console.log('[6] Target Player belongings after Swap : ', game.player[playerID].belongings);
						console.log('[6] Self belongings after Swap : ', game.player[game.curPlayer].belongings);
						var calls = [];
						for(var i = 0; i < game.winningCondition.length; i ++){
							calls.push(card[game.winningCondition[i]].func());
						}
						q.all(calls).then(callback); 
					});
					
				});
				
			});

		}
	
		
	};
	
	//Finished - Ching
	//tested - Ching
	//Finished - King
	//added callback
	//Client side done - Woody
	card[7].func = function(callback){	//回收一張牌
		var playerbelongings = [];
		var allCards = [];
		for (var i = 0; i < game.playerNum; i++){
			console.log("[7]playerbelongings before concat: " + playerbelongings);
			playerbelongings = playerbelongings.concat(game.player[i].belongings);
			console.log("[7]playerbelongings after concat: " + playerbelongings);
		}
		
//		console.log("[7]allcards before concat: " + allcards);
		allcards = game.rules.concat(game.winningCondition);
		allcards = allcards.concat(playerbelongings);
		console.log("[7]allcards after concat: " + allcards);
		
		if (allcards.length == 0){
			callback();
		}
		else{
			game.player[game.curPlayer].socket.emit("selectCard", "Please Select A Card!", allcards, function(cardID){
				console.log("[7]current player hands before push: " + game.player[game.curPlayer].hands);
				game.player[game.curPlayer].hands.push(cardID);
				game.player[game.curPlayer].socket.emit('updateHands', game.player[game.curPlayer].hands);
				console.log("[7]current player hands after push: " + game.player[game.curPlayer].hands);
				if (card[cardID].type == 2){
					console.log("[8]It is a belongings!");
					for (var i = 0; i < game.playerNum; i++){
						console.log("[7]CardID: " + cardID);
						console.log("[7]i: " + i);
						console.log("[7]belongings: " + game.player[i].belongings);
						console.log("[7]belongings type: " + typeof(game.player[i].belongings[0]));
						console.log("[7]CardID type: "+ typeof(cardID));
						console.log("[7]playerbelongings indexOF: " + game.player[i].belongings.indexOf(cardID));
						if (game.player[i].belongings.indexOf(cardID) != -1){
							console.log("[7]target player belongings before splice: " + game.player[i].belongings);
							game.player[i].belongings.splice(game.player[i].belongings.indexOf(cardID), 1);
							game.socket.emit('belongings', i, game.player[i].belongings);
							console.log("[7]target player belongings after splice: " + game.player[i].belongings);
							break;
						}
					}
				}
				else if (card[cardID].type == 3){
					console.log("[7]winning condition before splice: " + game.winningCondition);
					game.winningCondition.splice(game.winningCondition.indexOf(cardID), 1);
					game.socket.emit('winningCondition', game.winningCondition);
					console.log("[7]winning condition after splice: " + game.winningCondition);
					
				}
				else if (card[cardID].type == 4){
					console.log("[7]rules before splice: " + game.rules);
					game.rules.splice(game.rules.indexOf(cardID), 1);
					game.socket.emit('rules', game.rules);
					card[cardID].remove();
					console.log("[7]rules after splice: " + game.rules);
				}
				callback();
			});
		}
		
	};
	 
	//need checking of all players - ching
	//finished - ching
	//tested - ching
	//added callback
	//Client side done - Woody
	card[8].func = function(callback){	//棄一張持有物
		var playerIDs = getAllBelongings(game.curPlayer);
		console.log("[8] playerIDs: " + playerIDs);
		if (playerIDs.length == 0){
			callback();
			return;
		}
		else{
			game.player[game.curPlayer].socket.emit('selectPlayer', "Select A Player!", playerIDs, function(playerID){
				playerID = parseInt(playerID);
				console.log('[8] User Callback called with playerID: ', playerID);
				if (game.player[playerID].belongings.length == 0){
					console.log("[8]here!");//no belongings
					callback();
				}
				else{
					//select the belongings from the target
					game.player[game.curPlayer].socket.emit('selectCard', "Please select a belonging!" , game.player[playerID].belongings, function(cardID){
						console.log('[8] User Callback called with cardID: ', cardID);
						
						//discard it from the target
						console.log("[8] Player belongings before splice : " + game.player[playerID].belongings);
						game.player[playerID].belongings.splice(game.player[playerID].belongings.indexOf(cardID), 1);
						game.socket.emit('belongings', playerID, game.player[playerID].belongings);
						console.log("[8] Player belongings after splice : " + game.player[playerID].belongings);

						console.log("[8] before push discard : " + game.discard);
						game.discard.push(cardID);
						game.socket.emit('discard', game.discard);
						console.log("[8] after push discard : " + game.discard);
						callback();
					});	
				}
			});
		}	//select belongings from target
			//discard
	
	};
	
	//Finished - Woody
	//Client side done - Woody
	//added callback
	//tested - ching
	card[9].func = function(callback){	//財富重新分配
		var tmp = [];
		//push all the belongings into tmp
		for (var i = 0; i < game.playerNum; i++){
			console.log("[9]Player[" + i + "] : " + game.player[i].belongings);
			tmp = tmp.concat(game.player[i].belongings);
			game.player[i].belongings = [];
			game.socket.emit('belongings', i, game.player[i].belongings);
		}
		//shuffle temp
		tmp = game.shuffle(tmp);
		console.log("[9]After Shuffle : " + tmp);
		//派卡
		for (var i = 0, j = game.curPlayer; i < tmp.length; i++, j++){
			if (j >= game.playerNum)
				j = 0;
			game.player[j].belongings.push(tmp[i]);
			game.socket.emit('belongings', j, game.player[j].belongings);
			console.log("[9]Player[" + j + "] : " + game.player[j].belongings);
		}
		var calls = [];
		for(var i = 0; i < game.winningCondition.length; i ++){
			calls.push(card[game.winningCondition[i]].func());
		}
		q.all(calls).then(callback); 
	};
	
	//Problem - can't loop socket.emit to select multiple card
	//Not yet - Woody
	//finished - ching
	//tested - ching
	card[10].func = function(callback){	//簡化規則 - discard rules while < rules.length/2 or player select to end
		if(game.rules.length == 0){
			callback();
			return;
		}
		else{
			var half = Math.round(game.rules.length / 2);
			console.log("[10] half: ", + half);
			console.log("[10] rules: " + game.rules)
			console.log("[10] player socket: " + game.player[game.curPlayer]);
			game.player[game.curPlayer].socket.emit("selectMultiCard", "Please Select Rules To Discard!(Can Choose 0 To Half Of The Rules)", game.rules, 0, half, function(cardID){
				console.log("[10] return cards:" + cardID);
				for(var i = 0; i < cardID.length; i++){
					console.log("[10]Game Rules Before Splice: " + game.rules);
					game.rules.splice(game.rules.indexOf(cardID[i]), 1);
					card[cardID[i]].remove();
					console.log("[10]Game Rules After Splice: " + game.rules);
				}
				console.log("[10]rules: " + game.rules);
				game.socket.emit("rules", game.rules);
				game.socket.emit("discard", game.discard);
				if(callback){
					callback();
				}
			});
		}
	};
	
	//Not yet tested
	//Finished - Woody
	//Added callback - King
	//delete - ching
	card[11].func = function(callback){	//再來一次
		//create a new array for selection
		var tmp = [];
		for (var i = 0; i < game.discard.length; i++){
			if ((card[game.discard[i]].type == 1 || card[game.discard[i]].type == 4) && game.discard[i] != 11){
				tmp.push(game.discard[i]);
			}
		}
		//select action of rules in discard pile
		if (tmp.length == 0){ //no suitable card
			console.log('[11] No suitable card');
			callback();
		}
		else{
			game.player[game.curPlayer].socket.emit('selectCard', "Please Select A Rule Or An Action Card!", tmp, function(cardID){
				console.log('[11] User Callback called with data: ', cardID);
				card[cardID].func();
				callback();
			});
		}
	};
	
	//Finished - Woody
	//Client side done - Woody
	//Added callback - King
	//tested - ching
	card[12].func = function(callback){	//中樂透
		var n = 3 + game.expand;
		console.log("[12]Player hands before add : " + game.player[game.curPlayer].hands);
		
		//Check if there are enough card
		if(n > game.deck.length){
			var tmp_deck = game.discard.concat(game.deck);
			game.discard = [];
			//Shuffle card deck
			game.deck = game.shuffle(tmp_deck);
			console.log("[12]Deck after shuffle : " + game.deck);
		}
		
		for (var i = 0; i < n; i++){
			var tmp = game.drawCard();
			console.log('[12] Draw card');
			game.player[game.curPlayer].hands.push(tmp);
			game.player[game.curPlayer].socket.emit("drawCard", tmp);
		}
		console.log("[12]Player hands after add : " + game.player[game.curPlayer].hands);
		callback();
		
	};
	
	//Finished - Woody
	//Added callback
	//Client side done - Woody
	//tested - ching
	card[13].func = function(callback){	//交換手牌
		var tmp = [];
		for (var i = 0; i < game.playerNum; i++){
			if (i != game.curPlayer)
				tmp.push(i);
		}
		//select target
		game.player[game.curPlayer].socket.emit('selectPlayer', 'Please Select A Player', tmp, function(playerID){
			playerID = parseInt(playerID);
			console.log('[13] User Callback called with playerID:', playerID);
			//swap hands
			var tmp = [];
			console.log('[13] Hands of curPlayer[' + game.curPlayer + '] before swap:', game.player[game.curPlayer].hands);
			console.log('[13] Hands of playerID[' + playerID + '] before swap:', game.player[playerID].hands);
			tmp = game.player[game.curPlayer].hands;
			game.player[game.curPlayer].hands = game.player[playerID].hands;
			game.player[playerID].hands = tmp;
			game.player[game.curPlayer].socket.emit('updateHands', game.player[game.curPlayer].hands);
			game.player[playerID].socket.emit('updateHands', game.player[playerID].hands);
			console.log('[13] Hands of curPlayer after swap:', game.player[game.curPlayer].hands);
			console.log('[13] Hands of playerID after swap:', game.player[playerID].hands);
			callback();
		});
	};
	
	//finished - ching
	//tested - ching
	//Client side done - Woody
	card[14].func = function(callback){	//無極限
		for (var i = 0; i < game.rules.length; i++){
			console.log("[14] Rules: " + game.rules);
			console.log("[14] type of Rules[0]: " + typeof(game.rules[0]));
			if ((game.rules[i] >= 69 && game.rules[i] <= 71) || (game.rules[i] >= 80 && game.rules[i] <= 82)){
				console.log("[14] rules before splice: " + game.rules);
				game.discard.push(game.rules[i]);
				game.rules.splice(i,1);
				game.socket.emit('rules', game.rules);
				game.socket.emit('discard', game.discard);
				console.log("[14] rules after splice: " + game.rules);
				i--;
			}
		}
		callback();
	};
	
	//Problem - Need handle no rules
	//Added callback
	//Client side done - Woody
	//tested - ching
	card[15].func = function(callback){	//棄一張新規則
		var rules = game.rules;
		if(rules.length ==0){
			callback();
		}
		else{
			game.player[game.curPlayer].socket.emit("selectCard", "Please Select A Rule!", rules, function(ruleID){ 
				console.log("[15]rule ID from player: " + ruleID);
				
				console.log("[15]rules before splice: " + game.rules);
				game.rules.splice(game.rules.indexOf(ruleID), 1);
				game.socket.emit('rules', game.rules);
				console.log("[15]rules after splice: " + game.rules);
				
				console.log("[15] before push discard : " + game.discard);
				game.discard.push(ruleID);
				game.socket.emit('discard', game.discard);
				console.log("[15] after push discard : " + game.discard);
				
				card[ruleID].remove();
				callback();
			});
		}
		//select rules
		//discard
	};
	
	//Big Problem
	//Client side done - Woody
	//Added callback
	//tested - ching
	card[16].func = function(callback){	//亂抽稅
		var drawCard = 1 + game.expand;
		console.log("[16]drawCard: " + drawCard);
		for (var j = 0; j < drawCard; j++){
			for (var i = 0; i < game.playerNum; i++){
				if (i != game.curPlayer){
					if (game.player[i].hands.length == 0){
						console.log('[16] No more hands for player', i);
						break;
					}
					else{
						//select n cards 
						console.log("[16]inner loop i: " + i);
						getCard = game.player[i].hands[Math.floor(Math.random() * game.player[i].hands.length)];
						console.log("[16]inner loop getCard: " + getCard);
						
						//discard from target
						console.log("[16]inner loop player[i] hands before splice: " + game.player[i].hands);
						game.player[i].hands.splice(game.player[i].hands.indexOf(getCard),1);
						game.player[i].socket.emit('updateHands', game.player[i].hands);
						console.log("[16]inner loop player[i] hands after splice: " + game.player[i].hands);
						
						//push into player
						console.log("[16]inner loop before push: " + game.player[game.curPlayer].hands);
						game.player[game.curPlayer].hands.push(getCard);
						game.player[game.curPlayer].socket.emit('drawCard', getCard);
						console.log("[16]inner loop after push: " + game.player[game.curPlayer].hands);
					}
				}
			}
		}
		callback();
	};
	
	//Finished - King
	//Added callback
	//tested - ching
	card[17].func = function(callback){	//抽2打2
		var get = 2 + game.expand;
		var play = 2 + game.expand;
		var extraHands = [];
	
		//Check if there are enough card
		if(get > game.deck.length){
			var tmp_deck = game.discard.concat(game.deck);
			game.discard = [];
			//Shuffle card deck
			game.deck = game.shuffle(tmp_deck);
			console.log("[17]Deck after shuffle : " + game.deck);
		}
	
		//抽卡入extraHands
		for (var i = 0; i < get; i ++){
			extraHands.push(game.drawCard());
		}
		
		//Triger user to chooce card from cards
		//用卡
		game.player[game.curPlayer].socket.emit('selectCard', "Choose A Card And Use!", extraHands, function(cardID1){
			console.log('[3]User select 1st card : ' + cardID1);
			extraHands.splice(extraHands.indexOf(cardID1), 1);
			executeCard(cardID1, function(){
				game.player[game.curPlayer].socket.emit('selectCard', "Choose A Card And Use!", extraHands, function(cardID2){
					console.log('[3]User select 2nd card : ' + cardID2);
					extraHands.splice(extraHands.indexOf(cardID2), 1);
					if(play == 3){	//Need to play one more card
						executeCard(cardID2,
							game.player[game.curPlayer].socket.emit('selectCard', "Choose A Card And Use!", extraHands, function(cardID3){
								console.log('[3]User select 3nd card : ' + cardID3);
								extraHands.splice(extraHands.indexOf(cardID3), 1);
								console.log('[3]User select 3rd card : ' + cardID);
								//discard剩番果張
	
								executeCard(cardID3, callback);
							})
						);
					}
					else{
						executeCard(cardID2, callback);
					}
				})
			}
				
			);
		});

	};
	
	//Finished - ching
	//Added callback
	//Client side not yet done - Woody (drawcard?)
	//tested - ching
	card[18].func = function(callback){	//棄牌重抽
		var need_discard = game.player[game.curPlayer].hands;
		console.log("[18]need_discard: " + need_discard);
		var need_draw = game.player[game.curPlayer].hands.length;
		console.log("[18]need_draw: " + need_draw);
		
		for (var i = 0; i < need_discard.length; i++){
			console.log("[18] discard before push: " + game.discard);
			game.discard.push(need_discard[i]);
			game.socket.emit('discard', game.discard);
			console.log("[18] discard after push: " + game.discard);
		}
		/*for (var i = 0; i < game.player[game.curPlayer].hands; i++){
			game.player[game.curPlayer].hands.pop();
			game.player[game.curPlayer].socket.emit('updateHands', game.player[game.curPlayer].hands);
		}*/
		console.log("[18]current player hands before draw: " + game.player[game.curPlayer].hands);
		game.player[game.curPlayer].hands = [];
		game.player[game.curPlayer].socket.emit('updateHands', game.player[game.curPlayer].hands, function(){
			callback();
		});
		for (var i = 0; i < need_draw; i++){
			game.player[game.curPlayer].hands.push(game.drawCard());
		}
		game.player[game.curPlayer].socket.emit('updateHands', game.player[game.curPlayer].hands, function(){
			callback();
		});
		console.log("[18]current player hands after draw: " + game.player[game.curPlayer].hands);
		callback();
	};
	 
	//Finished - ching
	//tested - ching
	//Client side done - Woody
	//Added callback
	card[19].func = function(callback){	//重設規則	
		console.log("[19]game rule length before clear: " + game.rules.length);
		for (var i = 0; i < game.rules.length; i++){
			var remove = game.rules[i];
			console.log("[19]Remove: " + remove);
			console.log("[19]type of Remove: " + typeof(remove));
			card[remove].remove();
		}
		
		game.discard = game.discard.concat(game.rules);
		game.rules = [];
		
		game.socket.emit('rules', game.rules);
		game.socket.emit('discard', game.discard);
		console.log("[19]game rule length after clear: " + game.rules.length);
		callback();
	};




	/***************************************winningCondition********************************************/
	//No checking on when 1 player use card[4] [9] to steal belongings and cannot win - ching
	//No checking on play the winningCondition first and then play belongings cannot win - ching
	//Can win when having both belongings and play winningCondition
	//change winning condition have not push to discard
	//finished - ching
	//tested play it can show win- ching
	//Added callback
	card[39].func = function(callback){	//夜以繼日 = 太陽 + 月亮
		var have = 0;	//有幾多張啱嘅belongings
		var win = -1;	//index of winner player
		for (var i = 0; i < game.playerNum; i++){
			have = 0;
			console.log("[39] Player belonging: " + game.player[i].belongings);
			for (var j = 0; j < game.player[i].belongings.length; j++){
				if (game.player[i].belongings[j] == 29 || game.player[i].belongings[j] == 21)
					have++;
				}
			if (have == 2){
				console.log("[39] win before: " + win);
				win = i;
				console.log("[39] win after: " + win);
				break;
			}
		}
		console.log("[39] win before callback: " + win);
		console.log("[39] win type: " + typeof(win));
		//callback(win);
		
		//Return promise
		var deferred = q.defer();
		deferred.resolve(win);
		return deferred.promise;
		
	};
	//tested play it can show win- ching
	//Added callback
	card[40].func = function(callback){	//愛情無價 = 愛 + 金錢 22 33
		var have = 0;	//有幾多張啱嘅belongings
		var win = -1;	//index of winner player
		for (var i = 0; i < game.playerNum; i++){
			have = 0;
			for (var j = 0; j < game.player[i].belongings.length; j++){
				if (game.player[i].belongings[j] == 22 || game.player[i].belongings[j] == 33)
					have++;
				}
			if (have == 2)
				win = i;
			}
		//callback(win);
		
		//Return promise
		var deferred = q.defer();
		deferred.resolve(win);
		return deferred.promise;
	};
	//Added callback
	//tested play it can show win- ching
	card[41].func = function(callback){	//心智 = 愛 + 腦 22 28
		var have = 0;	//有幾多張啱嘅belongings
		var win = -1;	//index of winner player
		for (var i = 0; i < game.playerNum; i++){
			have = 0;
			for (var j = 0; j < game.player[i].belongings.length; j++){
				if (game.player[i].belongings[j] == 22 || game.player[i].belongings[j] == 28)
					have++;
				}
			if (have == 2)
				win = i;
			}
		//callback(win);
		
		//Return promise
		var deferred = q.defer();
		deferred.resolve(win);
		return deferred.promise;
	};
	//Added callback
	//tested play it can show win- ching
	card[42].func = function(callback){	//搖籃曲 = 音樂 + 睡眠 20 37
		var have = 0;	//有幾多張啱嘅belongings
		var win = -1;	//index of winner game.player
		for (var i = 0; i < game.playerNum; i++){
			have = 0;
			for (var j = 0; j < game.player[i].belongings.length; j++){
				if (game.player[i].belongings[j] == 20 || game.player[i].belongings[j] == 37)
					have++;
				}
			if (have == 2)
				win = i;
			}
		//callback(win);
		
		//Return promise
		var deferred = q.defer();
		deferred.resolve(win);
		return deferred.promise;
	};
	//Added callback
	//tested play it can show win- ching
	card[43].func = function(callback){	//通天眼 = 腦 + 眼睛 28 24
		var have = 0;	//有幾多張啱嘅belongings
		var win = -1;	//index of winner game.player
		for (var i = 0; i < game.playerNum; i++){
			have = 0;
			for (var j = 0; j < game.player[i].belongings.length; j++){
				if (game.player[i].belongings[j] == 28 || game.player[i].belongings[j] == 24)
					have++;
				}
			if (have == 2)
				win = i;
			}
		//callback(win);
		
		//Return promise
		var deferred = q.defer();
		deferred.resolve(win);
		return deferred.promise;
	};
	//Added callback
	//tested play it can show win- ching
	card[44].func = function(callback){	//巧克力醬 = 巧克力 + 太陽 36 29
		var have = 0;	//有幾多張啱嘅belongings
		var win = -1;	//index of winner game.player
		for (var i = 0; i < game.playerNum; i++){
			have = 0;
			for (var j = 0; j < game.player[i].belongings.length; j++){
				if (game.player[i].belongings[j] == 36 || game.player[i].belongings[j] == 29)
					have++;
				}
			if (have == 2)
				win = i;
			}
		//callback(win);
		
		//Return promise
		var deferred = q.defer();
		deferred.resolve(win);
		return deferred.promise;
	};
	//Added callback
	//tested play it can show win- ching
	card[45].func = function(callback){	//烤吐司 = 麵包 + 烤麵包機 23 26
		var have = 0;	//有幾多張啱嘅belongings
		var win = -1;	//index of winner game.player
		for (var i = 0; i < game.playerNum; i++){
			have = 0;
			for (var j = 0; j < game.player[i].belongings.length; j++){
				if (game.player[i].belongings[j] == 23 || game.player[i].belongings[j] == 26)
					have++;
				}
			if (have == 2)
				win = i;
			}
		//callback(win);
		
		//Return promise
		var deferred = q.defer();
		deferred.resolve(win);
		return deferred.promise;
	};
	//Added callback	
	//tested play it can show win- ching
	card[46].func = function(callback){	//中頭彩 = 夢 + 金錢 34 33
		var have = 0;	//有幾多張啱嘅belongings
		var win = -1;	//index of winner game.player
		for (var i = 0; i < game.playerNum; i++){
			have = 0;
			for (var j = 0; j < game.player[i].belongings.length; j++){
				if (game.player[i].belongings[j] == 34 || game.player[i].belongings[j] == 33)
					have++;
				}
			if (have == 2)
				win = i;
			}
		//callback(win);
		
		//Return promise
		var deferred = q.defer();
		deferred.resolve(win);
		return deferred.promise;
	};
	//Added callback
	//tested play it can show win- ching
	card[47].func = function(callback){	//魔境夢遊 = 睡眠 + 夢
		var have = 0;	//有幾多張啱嘅belongings
		var win = -1;	//index of winner game.player
		for (var i = 0; i < game.playerNum; i++){
			have = 0;
			for (var j = 0; j < game.player[i].belongings.length; j++){
				if (game.player[i].belongings[j] == 37 || game.player[i].belongings[j] == 34)
					have++;
				}
			if (have == 2)
				win = i;
			}
		//callback(win);
		
		//Return promise
		var deferred = q.defer();
		deferred.resolve(win);
		return deferred.promise;
	};
	//Added callback
	//tested play it can show win- ching
	card[48].func = function(callback){	//嬉皮 = 愛 + 和平
		var have = 0;	//有幾多張啱嘅belongings
		var win = -1;	//index of winner game.player
		for (var i = 0; i < game.playerNum; i++){
			have = 0;
			for (var j = 0; j < game.player[i].belongings.length; j++){
				if (game.player[i].belongings[j] == 22 || game.player[i].belongings[j] == 27)
					have++;
				}
			if (have == 2)
				win = i;
			}
		//callback(win);
		
		//Return promise
		var deferred = q.defer();
		deferred.resolve(win);
		return deferred.promise;
	};
	//Added callback
	//tested play it can show win- ching
	card[49].func = function(callback){	//巧克力麵包 = 巧克力 + 麵包
		var have = 0;	//有幾多張啱嘅belongings
		var win = -1;	//index of winner game.player
		for (var i = 0; i < game.playerNum; i++){
			have = 0;
			for (var j = 0; j < game.player[i].belongings.length; j++){
				if (game.player[i].belongings[j] == 36 || game.player[i].belongings[j] == 23)
					have++;
				}
			if (have == 2)
				win = i;
			}
		//callback(win);
		
		//Return promise
		var deferred = q.defer();
		deferred.resolve(win);
		return deferred.promise;
	};
	//Added callback
	//tested have 28 no 32 can win
	//tested have 28 and 32 cannot win
	//tested have 28 and 32 on field after getting 32 from field cannot win - ching
	card[50].func = function(callback){	//腦(嚴禁電視) = 腦 + 全場無電視
		var tv = 0;	//有無電視
		var win = -1;	//index of winner game.player
		for (var i = 0; i < game.playerNum; i++){
			for (var j = 0; j < game.player[i].belongings.length; j++){
				if (game.player[i].belongings[j] == 32)
					tv = 1;
				if (game.player[i].belongings[j] == 28)
					win = i;
			}
		}
		if (tv == 1){
			win = -1;
		}
		
		//Return promise
		var deferred = q.defer();
		deferred.resolve(win);
		return deferred.promise;
	};
	//Added callback
	//tested when 2 player have same hands, play the card will still go on but when draw card by another player cannot win - ching
	//have more than 10 and play can win 
	//Delete
	card[51].func = function(callback){	//10項全能 = 10張手牌
		var n = 10;
		var win = -1;	//index of winner game.player
		var max = -1;
		if (game.rules.indexOf(91)!=-1)	//通貨膨漲
			n++;
		for (var i = 0; i < game.playerNum; i++){
			if (game.player[i].hands.length == max)	//一樣手牌數
				win = -1;
			if (game.player[i].hands.length > max){
				win = i;
				max = game.player[i].hands.length;
				}
			}
		if (max >= n){
			callback(win);
		}
		else{
			callback(-1);
		}
	};
	//Added callback
	card[52].func = function(callback){	//一寸光陰一寸金 = 時間 + 金錢
		var have = 0;	//有幾多張啱嘅belongings
		var win = -1;	//index of winner game.player
		for (var i = 0; i < game.playerNum; i++){
			have = 0;
			for (var j = 0; j < game.player[i].belongings.length; j++){
				if (game.player[i].belongings[j] == 35 || game.player[i].belongings[j] == 33)
					have++;
				}
			if (have == 2)
				win = i;
			}
		//callback(win);
		
		//Return promise
		var deferred = q.defer();
		deferred.resolve(win);
		return deferred.promise;
	};
	//Added callback
	card[53].func = function(callback){	//派對時光 = 派對 + 時間
		var have = 0;	//有幾多張啱嘅belongings
		var win = -1;	//index of winner game.player
		for (var i = 0; i < game.playerNum; i++){
			have = 0;
			for (var j = 0; j < game.player[i].belongings.length; j++){
				if (game.player[i].belongings[j] == 25 || game.player[i].belongings[j] == 35)
					have++;
				}
			if (have == 2)
				win = i;
			}
		//callback(win);
		
		//Return promise
		var deferred = q.defer();
		deferred.resolve(win);
		return deferred.promise;
	};
	//Added callback
	card[54].func = function(callback){	//主題曲 = 電視 + 音樂
		var have = 0;	//有幾多張啱嘅belongings
		var win = -1;	//index of winner game.player
		for (var i = 0; i < game.playerNum; i++){
			have = 0;
			for (var j = 0; j < game.player[i].belongings.length; j++){
				if (game.player[i].belongings[j] == 32 || game.player[i].belongings[j] == 20)
					have++;
				}
			if (have == 2)
				win = i;
			}
		//callback(win);
		
		//Return promise
		var deferred = q.defer();
		deferred.resolve(win);
		return deferred.promise;
	};
	//Added callback
	card[55].func = function(callback){	//睡眠時間 = 睡眠 + 時間
		var have = 0;	//有幾多張啱嘅belongings
		var win = -1;	//index of winner game.player
		for (var i = 0; i < game.playerNum; i++){
			have = 0;
			for (var j = 0; j < game.player[i].belongings.length; j++){
				if (game.player[i].belongings[j] == 37 || game.player[i].belongings[j] == 35)
					have++;
				}
			if (have == 2)
				win = i;
			}
		//callback(win);
		
		//Return promise
		var deferred = q.defer();
		deferred.resolve(win);
		return deferred.promise;
	};
	//Added callback
	card[56].func = function(callback){	//巧克力牛奶 = 巧克力 + 牛奶
		var have = 0;	//有幾多張啱嘅belongings
		var win = -1;	//index of winner game.player
		for (var i = 0; i < game.playerNum; i++){
			have = 0;
			for (var j = 0; j < game.player[i].belongings.length; j++){
				if (game.player[i].belongings[j] == 36 || game.player[i].belongings[j] == 31)
					have++;
				}
			if (have == 2)
				win = i;
			}
		//callback(win);
		
		//Return promise
		var deferred = q.defer();
		deferred.resolve(win);
		return deferred.promise;
	};
	//Added callback
	card[57].func = function(callback){	//烘焙王 = 麵包 + 餅乾
		var have = 0;	//有幾多張啱嘅belongings
		var win = -1;	//index of winner game.player
		for (var i = 0; i < game.playerNum; i++){
			have = 0;
			for (var j = 0; j < game.player[i].belongings.length; j++){
				if (game.player[i].belongings[j] == 23 || game.player[i].belongings[j] == 30)
					have++;
				}
			if (have == 2)
				win = i;
			}
		//callback(win);
		
		//Return promise
		var deferred = q.defer();
		deferred.resolve(win);
		return deferred.promise;
	};
	//Added callback
	card[58].func = function(callback){	//太空科學 = 火箭 + 腦
		var have = 0;	//有幾多張啱嘅belongings
		var win = -1;	//index of winner game.player
		for (var i = 0; i < game.playerNum; i++){
			have = 0;
			for (var j = 0; j < game.player[i].belongings.length; j++){
				if (game.player[i].belongings[j] == 38 || game.player[i].belongings[j] == 28)
					have++;
				}
			if (have == 2)
				win = i;
			}
		//callback(win);
		
		//Return promise
		var deferred = q.defer();
		deferred.resolve(win);
		return deferred.promise;
	};
	//Added callback
	card[59].func = function(callback){	//5子登科 = 5張持有物
		var n = 5;
		var win = -1;	//index of winner game.player
		var max = -1;
		if (game.rules.indexOf(91)!=-1)	//通貨膨漲
			n++;
		for (var i = 0; i < game.playerNum; i++){
			if (game.player[i].belongings.length == max)	//一樣belongings
				win = -1;
			if (game.player[i].belongings.length > max){
				win = i;
				max = game.player[i].belongings.length;
				}
			}
		if (max >= n){
		}
		else{
			win = -1;
		}
		//callback(win);
		
		//Return promise
		var deferred = q.defer();
		deferred.resolve(win);
		return deferred.promise;
	};
	//Added callback
	card[60].func = function(callback){	//炒熱氣氛 = 音樂 + 派對
		var have = 0;	//有幾多張啱嘅belongings
		var win = -1;	//index of winner game.player
		for (var i = 0; i < game.playerNum; i++){
			have = 0;
			for (var j = 0; j < game.player[i].belongings.length; j++){
				if (game.player[i].belongings[j] == 20 || game.player[i].belongings[j] == 25)
					have++;
				}
			if (have == 2)
				win = i;
			}
		//callback(win);
		
		//Return promise
		var deferred = q.defer();
		deferred.resolve(win);
		return deferred.promise;
	};
	//Added callback
	card[61].func = function(callback){	//世界和平 = 夢 + 和平
		var have = 0;	//有幾多張啱嘅belongings
		var win = -1;	//index of winner game.player
		for (var i = 0; i < game.playerNum; i++){
			have = 0;
			for (var j = 0; j < game.player[i].belongings.length; j++){
				if (game.player[i].belongings[j] == 34 || game.player[i].belongings[j] == 27)
					have++;
				}
			if (have == 2)
				win = i;
			}
		//callback(win);
		
		//Return promise
		var deferred = q.defer();
		deferred.resolve(win);
		return deferred.promise;
	};
	//Added callback
	card[62].func = function(callback){	//牛奶餅乾 = 牛奶 + 餅乾
		var have = 0;	//有幾多張啱嘅belongings
		var win = -1;	//index of winner game.player
		for (var i = 0; i < game.playerNum; i++){
			have = 0;
			for (var j = 0; j < game.player[i].belongings.length; j++){
				if (game.player[i].belongings[j] == 31 || game.player[i].belongings[j] == 30)
					have++;
				}
			if (have == 2)
				win = i;
			}
		//callback(win);
		
		//Return promise
		var deferred = q.defer();
		deferred.resolve(win);
		return deferred.promise;
	};
	//Added callback
	card[63].func = function(callback){	//白日夢 = 夢 + 太陽
		var have = 0;	//有幾多張啱嘅belongings
		var win = -1;	//index of winner game.player
		for (var i = 0; i < game.playerNum; i++){
			have = 0;
			for (var j = 0; j < game.player[i].belongings.length; j++){
				if (game.player[i].belongings[j] == 34 || game.player[i].belongings[j] == 29)
					have++;
				}
			if (have == 2)
				win = i;
			}
		//callback(win);
		
		//Return promise
		var deferred = q.defer();
		deferred.resolve(win);
		return deferred.promise;
	};
	//Added callback
	card[64].func = function(callback){	//家電王 = 電視 + 烤麵包機
		var have = 0;	//有幾多張啱嘅belongings
		var win = -1;	//index of winner game.player
		for (var i = 0; i < game.playerNum; i++){
			have = 0;
			for (var j = 0; j < game.player[i].belongings.length; j++){
				if (game.player[i].belongings[j] == 32 || game.player[i].belongings[j] == 26)
					have++;
			}
			if (have == 2){
				win = i;
				break;
			}
				
		}
		//callback(win);
		
		//Return promise
		var deferred = q.defer();
		deferred.resolve(win);
		return deferred.promise;
	};
	//Added callback	
	card[65].func = function(callback){	//派對美食 = 派對 + (牛奶 || 餅乾 || 巧克力 || 麵包)
		var party = 0;	//有無派對
		var food = 0;	//有無食物
		var win = -1;	//index of winner game.player
		for (var i = 0; i < game.playerNum; i++){
			party = 0;
			food = 0;
			for (var j = 0; j < game.player[i].belongings.length; j++){
				if (game.player[i].belongings[j] == 25)
					party = 1;
				if (game.player[i].belongings[j] == 31 || game.player[i].belongings[j] == 30 || game.player[i].belongings[j] == 36 || game.player[i].belongings[j] == 23)
					food = 1;
				}
			if (party == 1 && food == 1)
				win = i;
			}
		//callback(win);
		
		//Return promise
		var deferred = q.defer();
		deferred.resolve(win);
		return deferred.promise;
	};
	//Added callback
	card[66].func = function(callback){	//情人眼裏出西施 = 眼睛 + 愛
		var have = 0;	//有幾多張啱嘅belongings
		var win = -1;	//index of winner game.player
		for (var i = 0; i < game.playerNum; i++){
			have = 0;
			for (var j = 0; j < game.player[i].belongings.length; j++){
				if (game.player[i].belongings[j] == 24 || game.player[i].belongings[j] == 22)
					have++;
			}
			if (have == 2){
				win = i;
				break;
			}
				
		}
		//callback(win);
		
		//Return promise
		var deferred = q.defer();
		deferred.resolve(win);
		return deferred.promise;
	};
	//Added callback
	card[67].func = function(callback){	//巧克力餅乾 = 巧克力 + 餅乾 
		var have = 0;	//有幾多張啱嘅belongings
		var win = -1;	//index of winner game.player
		for (var i = 0; i < game.playerNum; i++){
			have = 0;
			for (var j = 0; j < game.player[i].belongings.length; j++){
				if (game.player[i].belongings[j] == 36 || game.player[i].belongings[j] == 30)
					have++;
			}
			if (have == 2){
				win = i;
				break;
			}
				
		}
		//callback(win);
		
		//Return promise
		var deferred = q.defer();
		deferred.resolve(win);
		return deferred.promise;
	};
	//Added callback
	card[68].func = function(callback){	//登陸月球 = 火箭 + 月亮 
		var have = 0;	//有幾多張啱嘅belongings
		var win = -1;	//index of winner game.player
		for (var i = 0; i < game.playerNum; i++){
			have = 0;
			for (var j = 0; j < game.player[i].belongings.length; j++){
				if (game.player[i].belongings[j] == 38 || game.player[i].belongings[j] == 21)
					have++;
				}
			if (have == 2)
				win = i;
			}
		//callback(win);
		
		//Return promise
		var deferred = q.defer();
		deferred.resolve(win);
		return deferred.promise;
	};
	

/***************************************Rules********************************************/
	//finished - ching
	//Added promises
	//Client side done - Woody
	//tested can hold the limit - ching
	//tested can discard if played belongings first then play the rules - ching
	card[69].func = function(callback){	//持有物上限2
		var	the_promises = [];
		var n = 2 + game.expand;
		var current = 69;
		var playerIDs = [];

		console.log("[69] In card 69");
		var calls = [];
		for (var i = 0; i < game.playerNum; i++){
			if (i != game.curPlayer){
				console.log("[69Debug] game.player[" + i + "].belongings : " + game.player[i].belongings);
				if(game.player[i].belongings.length > n){
					playerIDs.push(i);
				}
			}
		}
		console.log("[69] Players that need to discard belongings : " + playerIDs);
		for(var i = 0; i < playerIDs.length; i ++){
			var deferred = q.defer();
			var need_discard = game.player[playerIDs[i]].belongings.length - n;
			console.log("[71] need discard: " + need_discard);
			var msg = "[Belonging limits] Please Discard " + need_discard + " Belongings";
			game.player[playerIDs[i]].socket.emit("selectMultiCard", msg, game.player[playerIDs[i]].belongings, need_discard, need_discard, 
				(function (){
					var cnt = i;
					var _deferred = deferred;
					return function(cardID){
						console.log("[69]player return cardID: " + cardID);
						for(var j=0; j < cardID.length; j++){
							console.log("[69]player belongings before splice: " + game.player[playerIDs[cnt]].belongings);
							game.player[playerIDs[cnt]].belongings.splice(game.player[playerIDs[cnt]].belongings.indexOf(cardID[j]),1);
							game.socket.emit('belongings', playerIDs[cnt], game.player[playerIDs[cnt]].belongings);
							console.log("[69]player belongings after splice: " + game.player[playerIDs[cnt]].belongings);
							
							console.log("[69]discard before push" + game.discard);
							game.discard.push(cardID[j]);
							game.socket.emit('discard', game.discard);
							console.log("[69]discard after push" + game.discard);
						}
						_deferred.resolve();
					};
				})() 
			);
			the_promises.push(deferred.promise);
		}
		for (var i = 69; i < 72 ; i++){
			if (i != current){
				removeduplicaterules(i);
			}
		}
		game.belongingslimit = n;
		
		return q.all(the_promises);
	};

	card[69].remove = function(){
		game.belongingslimit = -1;
	};
	 
	//finished - ching
	//not test - ching
	//Added promises
	//Client side done - Woody
	card[70].func = function(callback){	//持有物上限3
		var	the_promises = [];
		var n = 3 + game.expand;
		var current = 70;
		var playerIDs = [];

		console.log("[70] In card 70");
		var calls = [];
		for (var i = 0; i < game.playerNum; i++){
			if (i != game.curPlayer){
				console.log("[70Debug] game.player[" + i + "].belongings : " + game.player[i].belongings);
				if(game.player[i].belongings.length > n){
					playerIDs.push(i);
				}
			}
		}
		console.log("[70] Players that need to discard belongings : " + playerIDs);
		for(var i = 0; i < playerIDs.length; i ++){
			var deferred = q.defer();
			var need_discard = game.player[playerIDs[i]].belongings.length - n;
			console.log("[70] need discard: " + need_discard);
			var msg = "[Belonging limits] Please Discard " + need_discard + " Belongings";
			game.player[playerIDs[i]].socket.emit("selectMultiCard", msg, game.player[playerIDs[i]].belongings, need_discard, need_discard, 
				(function (){
					var cnt = i;
					var _deferred = deferred;
					return function(cardID){
						console.log("[70]player return cardID: " + cardID);
						for(var j=0; j < cardID.length; j++){
							console.log("[70]player belongings before splice: " + game.player[playerIDs[cnt]].belongings);
							game.player[playerIDs[cnt]].belongings.splice(game.player[playerIDs[cnt]].belongings.indexOf(cardID[j]),1);
							game.socket.emit('belongings', playerIDs[cnt], game.player[playerIDs[cnt]].belongings);
							console.log("[70]player belongings after splice: " + game.player[playerIDs[cnt]].belongings);
							
							console.log("[70]discard before push" + game.discard);
							game.discard.push(cardID[j]);
							game.socket.emit('discard', game.discard);
							console.log("[70]discard after push" + game.discard);
						}
						_deferred.resolve();
					};
				})() 
			);
			the_promises.push(deferred.promise);
		}
		for (var i = 69; i < 72 ; i++){
			if (i != current){
				removeduplicaterules(i);
			}
		}
		game.belongingslimit = n;
		
		return q.all(the_promises);
	};

	card[70].remove = function(){
		game.belongingslimit = -1;
	};
	
	//finished - ching
	//not test - ching
	//Added promises
	//Client side done - Woody
	card[71].func = function(callback){	//持有物上限4
		var	the_promises = [];
		var n = 4 + game.expand;
		var current = 71;
		var playerIDs = [];

		console.log("[71] In card 71");
		var calls = [];
		for (var i = 0; i < game.playerNum; i++){
			if (i != game.curPlayer){
				console.log("[71Debug] game.player[" + i + "].belongings : " + game.player[i].belongings);
				if(game.player[i].belongings.length > n){
					playerIDs.push(i);
				}
			}
		}
		console.log("[71] Players that need to discard belongings : " + playerIDs);
		for(var i = 0; i < playerIDs.length; i ++){
			var deferred = q.defer();
			var need_discard = game.player[playerIDs[i]].belongings.length - n;
			console.log("[71] need discard: " + need_discard);
			var msg = "[Belonging limits] Please Discard " + need_discard + " Belongings";
			game.player[playerIDs[i]].socket.emit("selectMultiCard", msg, game.player[playerIDs[i]].belongings, need_discard, need_discard, 
				(function (){
					var cnt = i;
					var _deferred = deferred;
					return function(cardID){
						console.log("[71]player return cardID: " + cardID);
						for(var j=0; j < cardID.length; j++){
							console.log("[71]player belongings before splice: " + game.player[playerIDs[cnt]].belongings);
							game.player[playerIDs[cnt]].belongings.splice(game.player[playerIDs[cnt]].belongings.indexOf(cardID[j]),1);
							game.socket.emit('belongings', playerIDs[cnt], game.player[playerIDs[cnt]].belongings);
							console.log("[71]player belongings after splice: " + game.player[playerIDs[cnt]].belongings);
							
							console.log("[71]discard before push" + game.discard);
							game.discard.push(cardID[j]);
							game.socket.emit('discard', game.discard);
							console.log("[71]discard after push" + game.discard);
						}
						_deferred.resolve();
					};
				})() 
			);
			the_promises.push(deferred.promise);
		}
		for (var i = 69; i < 72 ; i++){
			if (i != current){
				removeduplicaterules(i);
			}
		}
		game.belongingslimit = n;		
		return q.all(the_promises);
	};

	card[71].remove = function(){
		game.belongingslimit = -1;
	};
	
	//finished - ching
	//not test - ching
	
	card[72].func = function(callback){	//打2張牌
		var n = 2  + game.expand;
		var current = 72;
		
		for (var i = 72; i < 77 ; i++){
			if (i != current){
				removeduplicaterules(i);
			}
		}

		game.playCardNumber = n;
		console.log("[72]game play card number: " + game.playCardNumber);
		
		//Return promise
		var deferred = q.defer();
		deferred.resolve();
		return deferred.promise;

	};
	card[72].remove = function(callback){
		game.playCardNumber = 1;
	};
	
	//finished - ching
	//not test - ching
	//tested play this card first and then remove by [15] can still play 1 more card - ching
	//tested [73]-[76] can replace duplicate and limit the play card number - ching
	card[73].func = function(callback){	//打3張牌
		var n = 3 + game.expand;
		var current = 73;
		
		
		for (var i = 72; i < 77 ; i++){
			if (i != current){
				removeduplicaterules(i);
			}
		}

		game.playCardNumber = n;
		console.log("[73]Player player card number: " + game.playCardNumber);
		
		//Return promise
		var deferred = q.defer();
		deferred.resolve();
		return deferred.promise;
		
	};
	card[73].remove = function(callback){
		game.playCardNumber = 1;
	};
	
	//finished - ching
	//not test - ching

	card[74].func = function(callback){	//打4張牌
		var n = 4 + game.expand;
		var current = 74;
		console.log("[74]game play card number: " + game.playCardNumber);
		
		for (var i = 72; i < 77 ; i++){
			if (i != current){
				removeduplicaterules(i);
			}
		}

		game.playCardNumber = n;
		
		//Return promise
		var deferred = q.defer();
		deferred.resolve();
		return deferred.promise;
	};
	card[74].remove = function(callback){
		game.playCardNumber = 1;
	};
	
	//finished - ching
	//not test - ching
	//tested can keep limit one - ching
	card[75].func = function(callback){	//打到剩1張
		var current = 75;
		console.log("[75]Before set - game.playthelastcard : " + game.playthelastcard);
		
		for (var i = 72; i < 77 ; i++){
			if (i != current){
				removeduplicaterules(i);
			}
		}
		game.playthelastcard = 1 + game.expand;
		console.log("[75]After set - game.playthelastcard : " + game.playthelastcard);
		//Return promise
		var deferred = q.defer();
		deferred.resolve();
		return deferred.promise;
	};
	card[75].remove = function(callback){
		game.playthelastcard = -1;
		console.log("[75]playthelastcard: " + game.playthelastcard);
		console.log("[75]Executed!");
	};
	
	//finished - ching
	//not test - ching
	
	card[76].func = function(callback){	//打光手牌
		
		var current = 76;
		
		
		for (var i = 72; i < 77 ; i++){
			if (i != current){
				removeduplicaterules(i);
			}
		}

		game.playthelastcard = 0;
		console.log("[76]playthelastcard : " + game.playthelastcard);
		//Return promise
		var deferred = q.defer();
		deferred.resolve();
		return deferred.promise;
	};
	card[76].remove = function(callback){
		game.playthelastcard = -1;
	};
	
	//need draw if not draw enough
	//finished - ching
	//not test - ching
	//added callback
	//Client side done - Woody
	//tested when played can draw card - ching
	//tested [77] - [79] can replace duplicate and will not draw if last draw > n - ching
	card[77].func = function(callback){	//抽3張牌

		var lastdraw = game.drawCardNumber;
		var current = 77;
		console.log("[77]lastdraw: " + lastdraw);

		var n = 3 + game.expand;
		
		if (lastdraw < n){
			var need_draw = n - lastdraw;
			for (var i = 0; i < need_draw; i++){
				var cardID = game.drawCard();
				console.log("[77]current player hands before push: " + game.player[game.curPlayer].hands);
				game.player[game.curPlayer].hands.push(cardID);
				console.log("[77]current player hands after push: " + game.player[game.curPlayer].hands);
				game.player[game.curPlayer].socket.emit('drawCard', cardID);
				
			}
		}
		
		
		
		for (var i = 77; i < 80 ; i++){
			if (i != current){
				removeduplicaterules(i);
			}
		}
		game.drawCardNumber = n;
		
		//Return promise
		var deferred = q.defer();
		deferred.resolve();
		return deferred.promise;
		
	};
	
	card[77].remove = function(callback){
		game.drawCardNumber = 1;
	};
	
	//finished - ching
	//not test - ching
	//added callback
	//Client side done - Woody
	card[78].func = function(callback){	//抽4張牌
		var lastdraw = game.drawCardNumber;
		var current = 78;
		console.log("[78]lastdraw: " + lastdraw);
		
		var n = 4 + game.expand;
			
		if (lastdraw < n){
			var need_draw = n - lastdraw;
			for (var i = 0; i < need_draw; i++){
				var cardID = game.drawCard();
				game.player[game.curPlayer].socket.emit('drawCard', cardID);
				console.log("[78]current player hands before push: " + game.player[game.curPlayer].hands);
				game.player[game.curPlayer].hands.push(cardID);
				console.log("[78]current player hands after push: " + game.player[game.curPlayer].hands);
			}
		}
		
		
		for (var i = 77; i < 80 ; i++){
			if (i != current){
				removeduplicaterules(i);
			}
		}
		game.drawCardNumber = n;
		
		//Return promise
		var deferred = q.defer();
		deferred.resolve();
		return deferred.promise;
	};
	
	card[78].remove = function(callback){
		game.drawCardNumber = 1;
	};
	
	//not test - ching
	//added callback
	//Client side done - Woody
	card[79].func = function(callback){	//抽5張牌
		var lastdraw = game.drawCardNumber;
		var current = 79;
		console.log("[79]lastdraw: " + lastdraw);
		
		var n = 5 + game.expand;
			
		if (lastdraw < n){
			var need_draw = n - lastdraw;
			for (var i = 0; i < need_draw; i++){
				var cardID = game.drawCard();
				game.player[game.curPlayer].socket.emit('drawCard', cardID);
				console.log("[79]current player hands before push: " + game.player[game.curPlayer].hands);
				game.player[game.curPlayer].hands.push(cardID);
				console.log("[79]current player hands after push: " + game.player[game.curPlayer].hands);
			}
		}
		
		
		for (var i = 77; i < 80 ; i++){
			if (i != current){
				removeduplicaterules(i);
			}
		}
		
		game.drawCardNumber = n;
		
		//Return promise
		var deferred = q.defer();
		deferred.resolve();
		return deferred.promise;
	};
	
	card[79].remove = function(callback){
		game.drawCardNumber = 1;
	};
	
	//Not yet test
	//Finished - Woody
	//added callback
	//Client side done - Woody
	card[80].func = function(callback){	//手牌上限0
		console.log("[80] Inside card 80 ");
		var	the_promises = [];
		var n = 0 + game.expand;
		var current = 80;
	
		for (var i = 0; i < game.playerNum; i++){
			if (i != game.curPlayer && game.player[i].hands.length > n){
				var deferred = q.defer();
				var need_discard = game.player[i].hands.length - n;
				var msg = "[Hands limits] Please Discard " + need_discard + " Hands";
				console.log("[80] Send discard to player " + i);
				game.player[i].socket.emit("selectMultiCard", msg, game.player[i].hands, need_discard, need_discard, 
					(function(){
						var cnt = i;
						var _deferred = deferred;
						return function(cardID){
							console.log("[80]player return cardID: " + cardID);
							for(var j=0; j < cardID.length; j++){
								console.log("[80]player hands before splice: " + game.player[cnt].hands);
								game.player[cnt].hands.splice(game.player[cnt].hands.indexOf(cardID[j]),1);
								console.log("[80]player hand after splice: " + game.player[cnt].hands);
								
								console.log("[80]discard before push: " + game.discard);
								game.discard.push(cardID[j]);
								console.log("[80]discard after push: " + game.discard);
							}
							game.socket.emit('discard', game.discard);
							game.player[cnt].socket.emit('updateHands', game.player[cnt].hands);
							_deferred.resolve();
							console.log("[80]Player " + cnt + " Resolved");
						};
					})()
					
				);
				the_promises.push(deferred.promise);
			}
		}
		for (var i = 80; i < 83 ; i++){
			if (i != current){
				removeduplicaterules(i);
			}
		}
		game.handslimit = n;
		
		return q.all(the_promises);
	
		
	};
	card[80].remove = function(callback){
		game.handslimit = -1;
	};
	
	//Not yet test
	//Finished - Woody
	//added callback
	//Client side done - Woody
	card[81].func = function(callback){	//手牌上限1
		console.log("[81] Inside card 80 ");
		var	the_promises = [];
		var n = 1 + game.expand;
		var current = 81;
	
		for (var i = 0; i < game.playerNum; i++){
			if (i != game.curPlayer && game.player[i].hands.length > n){
				var deferred = q.defer();
				var need_discard = game.player[i].hands.length - n;
				var msg = "[Hands limits] Please Discard " + need_discard + " Hands";
				console.log("[81] Send discard to player " + i);
				game.player[i].socket.emit("selectMultiCard", msg, game.player[i].hands, need_discard, need_discard, 
					(function(){
						var cnt = i;
						var _deferred = deferred;
						return function(cardID){
							console.log("[81]player return cardID: " + cardID);
							for(var j=0; j < cardID.length; j++){
								console.log("[81]player hands before splice: " + game.player[cnt].hands);
								game.player[cnt].hands.splice(game.player[cnt].hands.indexOf(cardID[j]),1);
								console.log("[81]player hand after splice: " + game.player[cnt].hands);
								
								console.log("[81]discard before push: " + game.discard);
								game.discard.push(cardID[j]);
								console.log("[81]discard after push: " + game.discard);
							}
							game.socket.emit('discard', game.discard);
							game.player[cnt].socket.emit('updateHands', game.player[cnt].hands);
							_deferred.resolve();
							console.log("[81]Player " + cnt + " Resolved");
						};
					})()
					
				);
				the_promises.push(deferred.promise);
			}
		}
		for (var i = 80; i < 83 ; i++){
			if (i != current){
				removeduplicaterules(i);
			}
		}
		game.handslimit = n;
		
		return q.all(the_promises);
	
		
	};
	card[81].remove = function(callback){
		game.handslimit = -1;
	};
	
	//Not yet test
	//Finished - Woody
	//Client side done - Woody
	card[82].func = function(callback){	//手牌上限2
		console.log("[82] Inside card 80 ");
		var	the_promises = [];
		var n = 2 + game.expand;
		var current = 82;
	
		for (var i = 0; i < game.playerNum; i++){
			if (i != game.curPlayer && game.player[i].hands.length > n){
				var deferred = q.defer();
				var need_discard = game.player[i].hands.length - n;
				var msg = "[Hands limits] Please Discard " + need_discard + " Hands";
				console.log("[82] Send discard to player " + i);
				game.player[i].socket.emit("selectMultiCard", msg, game.player[i].hands, need_discard, need_discard, 
					(function(){
						var cnt = i;
						var _deferred = deferred;
						return function(cardID){
							console.log("[82]player return cardID: " + cardID);
							for(var j=0; j < cardID.length; j++){
								console.log("[82]player hands before splice: " + game.player[cnt].hands);
								game.player[cnt].hands.splice(game.player[cnt].hands.indexOf(cardID[j]),1);
								console.log("[82]player hand after splice: " + game.player[cnt].hands);
								
								console.log("[82]discard before push: " + game.discard);
								game.discard.push(cardID[j]);
								console.log("[82]discard after push: " + game.discard);
							}
							game.socket.emit('discard', game.discard);
							game.player[cnt].socket.emit('updateHands', game.player[cnt].hands);
							_deferred.resolve();
							console.log("[82]Player " + cnt + " Resolved");
						};
					})()
					
				);
				the_promises.push(deferred.promise);
			}
		}
		for (var i = 80; i < 83 ; i++){
			if (i != current){
				removeduplicaterules(i);
			}
		}
		game.handslimit = n;
		
		return q.all(the_promises);
	
		
	};
	card[82].remove = function(callback){
		game.handslimit = -1;
	};
	

	//Not Yet Test
	//Finished - Woody
	//Client side done - Woody
	//Delete This Card
	card[83].func = function(callback){	//改打為抽
		var n = game.playCardNumber - game.curPlayerplayed;
		if (game.playthelastcard != -1){
			n = game.player[curPlayer].hands.length - game.playthelastcard;
			for (var i = 0; i < n; i++){
				game.player[game.curPlayer].hands.push(game.drawCard());
				game.player[game.curPlayer].socket.emit('updateHands', game.player[game.curPlayer].hands);
				console.log('[83] Draw Card');
			}
			game.curPlayerplayed = game.player[curPlayer].hands.length - game.playthelastcard;
		}
		else{
			for (var i = 0; i < n; i++){
				var cardID = game.drawCard();
				game.player[game.curPlayer].socket.emit('drawCard', cardID);
				game.player[game.curPlayer].hands.push(cardID);
				console.log('[83] Draw Card');
			}
			game.curPlayerplayed = game.playCardNumber;
		}
		if(callback){
			callback();
		}
	};
	card[83].remove = function(){
	};
	
	//Not Yet Test
	//Finished - Woody
	card[84].func = function(callback){	//錦上添花
		var max = -1;
		var number = -1;
		for (var i = 0; i < game.playerNum; i++){
			if (game.player[i].belongings.length > number){
				number = game.player[i].belongings.length;
				max = i;
			}
			else if (game.player[i].belongings.length == number)
				max = -1;
			console.log('[84] Current largest number of belongings:', number);
			console.log('[84] Current Selected playerID:', max);
		}
		console.log('[84] The original number of cards current player can play:', game.playCardNumber - game.curPlayerplayed);
		if (game.curPlayer == max)
			game.curPlayerplayed -= (1 + game.expand);
		console.log('[84] The new number of cards current player can play:', game.playCardNumber - game.curPlayerplayed);
		
		//Return promise
		var deferred = q.defer();
		deferred.resolve();
		return deferred.promise;
	};
	card[84].remove = function(){
	};
	
	//Finished - King 
	//No need to have any function
	//Will Check inside game flow
	//added callback
	//real finished - Woody
	card[85].func = function(callback){	//雙重議程
		console.log("[85]Inside card 85");
		//Return promise
		var deferred = q.defer();
		deferred.resolve();
		return deferred.promise;
	};
	card[85].remove = function(callback){
		if(game.winningCondition.length > 1){
			game.player[game.curPlayer].socket.emit("selectCard", "[WinningCondition Limit]Discard a winningCondition", game.winningCondition, function(removeCardID){
				console.log("[85]Current player select discard winningCondition : " + removeCardID);	
							
				//Remove old card 
				game.winningCondition.splice(game.winningCondition.indexOf(removeCardID), 1);
				game.discard.push(removeCardID);
				game.socket.emit('discard', game.discard);
							
				//Update Room
				console.log("Adding winningCondition : " + game.winningCondition + " to main page");
				game.socket.emit('winningCondition', game.winningCondition);
			});
		}
	};
	

	//Not Yet Test
	//Finished - Woody
	//Client side done - Woody
	//Delete
	card[86].func = function(callback){	//重新做人
		if (game.playCardNumber - game.curPlayerplayed == 1 && game.player[game.curPlayer].hands.length > 0){
			game.player[game.curPlayer].hands = [];
			for (var i = 0; i < 3 + game.expand; i++){
				var cardID = game.drawCard();
				game.player[game.curPlayer].socket.emit('drawCard', cardID);
				game.player[game.curPlayer].hands.push(cardID);
				console.log('[86] Draw card');
			}
			game.curPlayerplayed++;
		}
		callback();
	};
	card[86].remove = function(){
	};
	
	//Finished - Woody
	//Client side done - Woody
	//Will check in gameFlow
	card[87].func = function(callback){	//清寒補助

		//Return promise
		var deferred = q.defer();
		deferred.resolve();
		return deferred.promise;
	};
	card[87].remove = function(){
	};
	
	//Problem - can't loop socket.emit to select multiple card
	//Not yet - Woody
	//Maybe not include this card
	//Delete
	card[88].func = function(callback){	//廢物利用
		var n = 0;
		//Check 
		for (var i = 0; i < n; i++){}
			//抽牌
	};
	card[88].remove = function(){
	};
	
	//Problem - How to use the card?
	//Not yet - Woody
	//added callback
	//Delete
	card[89].func = function(callback){	//聽天由命
		var n = 1 + game.expand;
		var target = game.nextPlayer;
		if (target == game.playerNum)
			target = 0;
		if (game.playCardNumber <= n){
			console.log('[89] PlayCardNumber not enough');
			callback();
			return;
		}
		else{
			game.player[target].socket.emit('selectCard', game.player[game.curPlayer].hands, function(cardID){
				console.log('[89]User Callback called with cardID: ', cardID);
				callback();
			});
		}
	};
	card[89].remove = function(){
	};
	
	//Not Tested
	//Finished - Woody
	//Added callback
	//Client side done - Woody
	//Delete
	card[90].func = function(callback){	//資源再生
		var n = 3 + game.expand;
		if (game.player[game.curPlayer].belongings.length == 0){
			callback();
			return;
		}
		else{
			//select and discard a belonging
			game.player[game.curPlayer].socket.emit('selectCard', game.player[game.curPlayer].belongings, function(cardID){
				console.log('[90] User Callback called with cardID: ', cardID);
				game.player[game.curPlayer].belongings.splice(game.player[game.curPlayer].belongings.indexOf(cardID), 1);
				game.addDiscard(cardID);
				game.socket.emit('belongings', curPlayer, game.player[game.curPlayer].belongings);
				game.socket.emit('discard', game.discard);
				for (var i = 0; i < n; i++){
					var cardID = game.drawCard();
					console.log('[90] Draw card');
					game.player[game.curPlayer].hands.push(cardID);
					game.player[game.curPlayer].socket.emit('drawCard', cardID);
				}
				callback();
			});
		}
	};
	card[90].remove = function(){
	};
	
	//Not Test
	//Finished - Woody
	card[91].func = function(callback){	//通貨膨脹
		if (game.expand == 0 && game.party == 0){
			if (game.belongingslimit != -1)
				game.belongingslimit++;
			game.playCardNumber++;
			if (game.playthelastcard != -1 && game.playthelastcard != 0)
				game.playthelastcard++;
			game.drawCardNumber++;
			if (game.handslimit != -1)
				game.handslimit++;
			var cardID = game.drawCard();
			game.player[game.curPlayer].socket.emit('drawCard', cardID);
			console.log("[91]current player hands before push: " + game.player[game.curPlayer].hands);
			game.player[game.curPlayer].hands.push(cardID);
			console.log("[91]current player hands after push: " + game.player[game.curPlayer].hands);
		}
		else if (game.expand == 0 && game.party == 1){
			if (game.belongingslimit != -1)
				game.belongingslimit++;
			game.playCardNumber += 2;
			if (game.playthelastcard != -1 && game.playthelastcard != 0)
				game.playthelastcard++;
			game.drawCardNumber += 2;
			if (game.handslimit != -1)
				game.handslimit++;
			var cardID = game.drawCard();
			game.player[game.curPlayer].socket.emit('drawCard', cardID);
			console.log("[91]current player hands before push: " + game.player[game.curPlayer].hands);
			game.player[game.curPlayer].hands.push(cardID);
			cardID = game.drawCard();
			game.player[game.curPlayer].socket.emit('drawCard', cardID);
			game.player[game.curPlayer].hands.push(cardID);
			console.log("[91]current player hands after push: " + game.player[game.curPlayer].hands);
		}
		console.log('[91] game.party =',game.party);
		console.log('[91] game.belongingslimit =',game.belongingslimit);
		console.log('[91] game.playCardNumber =',game.playCardNumber);
		console.log('[91] game.playthelastcard =',game.playthelastcard);
		console.log('[91] game.drawCardNumber =',game.drawCardNumber);
		console.log('[91] game.handslimit =',game.handslimit);
		game.expand = 1;
		
		//Return promise
		var deferred = q.defer();
		deferred.resolve();
		return deferred.promise;
	};
	card[91].remove = function(){
		if (game.expand == 1 && game.party == 0){
			if (game.belongingslimit != -1)
				game.belongingslimit--;
			if (game.playCardNumber != 1)
				game.playCardNumber--;
			if (game.playthelastcard != -1 && game.playthelastcard != 0)
				game.playthelastcard--;
			if (game.drawCardNumber != 1)
				game.drawCardNumber--;
			if (game.handslimit != -1)
				game.handslimit--;
		}
		else if (game.expand == 1 && game.party == 1){
			if (game.belongingslimit != -1)
				game.belongingslimit--;
			if (game.playCardNumber != 1)
				game.playCardNumber -= 2;
			if (game.playthelastcard != -1 && game.playthelastcard != 0)
				game.playthelastcard--;
			if (game.playCardNumber != 1)
				game.drawCardNumber -= 2;
			if (game.handslimit != -1)
				game.handslimit--;
		}
		console.log('[91] game.party =',game.party);
		console.log('[91] game.belongingslimit =',game.belongingslimit);
		console.log('[91] game.playCardNumber =',game.playCardNumber);
		console.log('[91] game.playthelastcard =',game.playthelastcard);
		console.log('[91] game.drawCardNumber =',game.drawCardNumber);
		console.log('[91] game.handslimit =',game.handslimit);
		game.expand = 0;
	};
	
	//Not Test
	//Finished -- Woody
	card[92].func = function(){	//派對優惠
		if (game.party == 0 && game.expand == 0){
			game.playCardNumber++;
			game.drawCardNumber++;
		}
		if (game.party == 0 && game.expand == 1){
			game.playCardNumber += 2;
			game.drawCardNumber += 2;
		}
		console.log('[92] game.expand =',game.expand);
		console.log('[92] game.belongingslimit =',game.belongingslimit);
		console.log('[92] game.playCardNumber =',game.playCardNumber);
		console.log('[92] game.playthelastcard =',game.playthelastcard);
		console.log('[92] game.drawCardNumber =',game.drawCardNumber);
		console.log('[92] game.handslimit =',game.handslimit);
		game.party = 1;
		
		//Return promise
		var deferred = q.defer();
		deferred.resolve();
		return deferred.promise;
	};
	card[92].remove = function(){
		if (game.party == 1 && game.expand == 0){
			game.playCardNumber--;
			game.drawCardNumber--;
		}
		if (game.party == 1 && game.expand == 1){
			game.playCardNumber -= 2;
			game.drawCardNumber -= 2;
		}
		console.log('[92] game.expand =',game.expand);
		console.log('[92] game.belongingslimit =',game.belongingslimit);
		console.log('[92] game.playCardNumber =',game.playCardNumber);
		console.log('[92] game.playthelastcard =',game.playthelastcard);
		console.log('[92] game.drawCardNumber =',game.drawCardNumber);
		console.log('[92] game.handslimit =',game.handslimit);
		game.party = 0;
	};
	
	//Finished, but need client side function to trigger this function - King
	//Should have no expand
	//Delete
	card[93].func = function(callback){	//神來一筆
		var tmp = game.drawCard();
		console.log('[93] Drawed CardID:', tmp);
		executeCard(tmp, callback);
		//if (game.expand == 1)
			//tmp = game.drawCard();
	};
	card[93].remove = function(){
	};
	
	//Finished - Woody
	//Added callback
	//Client side done - Woody
	card[94].func = function(callback){	//雪中送炭
		var min = -1;
		var number = 999;
		for (var i = 0; i < game.playerNum; i++){
			if (game.player[i].belongings.length < number){
				number = game.player[i].belongings.length;
				min = i;
			}
			else if (game.player[i].belongings.length == number)
				min = -1;
			console.log('[94] Current smallest number of belongings:', number);
			console.log('[94] Current Selected playerID:', min);
		}
		console.log('[94] The hands of current player before checking:', game.player[game.curPlayer].hands);
		if (game.curPlayer == min){
			for (var i = 0; i < 1 + game.expand; i ++){
				var cardID = game.drawCard();
				console.log('[94] Draw card');
				game.player[game.curPlayer].socket.emit('drawCard', cardID);
				game.player[game.curPlayer].hands.push(cardID);
			}
		}
		console.log('[94] The hands of current player after checking:', game.player[game.curPlayer].hands);
		//Return promise
		var deferred = q.defer();
		deferred.resolve();
		return deferred.promise;
	};
	card[94].remove = function(){
	};
	
	return card;
}