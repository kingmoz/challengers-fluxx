var socketio = require('socket.io');
var	game = require('./game'),
	q = require("q"),
	initCard = require('./initCard').initCard;

var num_ready = new Array();

function seq(calls){
	var result = q();
	calls.forEach(function (f) {
		result = result.then(f);
	});
	return result;
}

	
module.exports = function(server){
	io = socketio(server)
	
	var players = new Array();
	var gameObj;
	var card;
	var roomIDs = [];
	
	
	
	function toNextPlayer(){
		console.log("-------To next player-------");
		//Change next player number
		gameObj.curPlayer = gameObj.nextPlayer;
		if(gameObj.nextPlayer + 1 == gameObj.playerNum){
			gameObj.nextPlayer = 0;
		}
		else{
			gameObj.nextPlayer++;
		} 
		gameObj.curPlayerplayed = 0;
		
		var calls = [];
		
		console.log('[87] gameObj.rules.indexOf(87):', gameObj.rules.indexOf(87));
		var extra = 0;
		if (gameObj.rules.indexOf(87) > -1 && gameObj.player[gameObj.curPlayer].hands.length == 0){
			var extra = 3 + gameObj.expand;
		}
		if (gameObj.rules.indexOf(75) > -1 && gameObj.player[gameObj.curPlayer].hands.length == 0 && gameObj.rules.indexOf(87) == -1 && gameObj.rules.indexOf(77) == -1 && gameObj.rules.indexOf(78) == -1 && gameObj.rules.indexOf(79) == -1)
			var extra = 1;
		console.log("[yourTurn]User drawCardNumber : " + (gameObj.drawCardNumber + extra));
		var tmp = [];
		//Check if there are still cards remaining
		if(gameObj.drawCardNumber + extra > gameObj.deck.length){
			console.log("All card used up");
			var drawXmoreCard = gameObj.drawCardNumber + extra - gameObj.deck.length;
			for(var i = 0; i < gameObj.deck.length; i ++){
				var tmpCardID = gameObj.drawCard();
				tmp.push(tmpCardID);
				gameObj.player[gameObj.curPlayer].hands.push(tmpCardID);
			}
			//Remove cards from discard and put into deck
			var tmp_deck = gameObj.discard;
			gameObj.discard = [];
			//Shuffle card deck
			gameObj.deck = gameObj.shuffle(tmp_deck);
			console.log("Deck after shuffle : " + gameObj.deck);
			//Draw remaining cards
			for(var i = 0; i < drawXmoreCard; i ++){
				var tmpCardID = gameObj.drawCard();
				tmp.push(tmpCardID);
				gameObj.player[gameObj.curPlayer].hands.push(tmpCardID);
			}
			
		}
		else{
			for(var i = 0; i < gameObj.drawCardNumber + extra; i ++){
				var tmpCardID = gameObj.drawCard();
				tmp.push(tmpCardID);
				gameObj.player[gameObj.curPlayer].hands.push(tmpCardID);
			}
		}
		//Send hands number to client
		var playerHandsNum = [];
		for(var i = 0; i < gameObj.playerNum; i++){
			playerHandsNum.push(gameObj.player[i].hands.length); 
		}
		gameObj.socket.emit('playerHandsNum', playerHandsNum);
		
		//Check if someone wins
		var winCalls = [];
		for(var i = 0; i < gameObj.winningCondition.length; i ++){
			//card[gameObj.winningCondition[i]].func(checkWin);
			winCalls.push(card[gameObj.winningCondition[i]].func());
		}
		q.all(winCalls).then(checkWin).then(function(){
			console.log("[ToNextPlayer]Inside winCalls Check");
			//Check rules and then go to next Turn
			for(var i = 0; i < gameObj.rules.length; i ++){
				calls.push(card[gameObj.rules[i]].func);
			}				
			seq(calls).then(function(value){ 
				console.log("[yourTurn]User draw card : " + tmp);
				console.log("[yourTurn]Inside Rules Check");
				gameObj.socket.emit('roomCurrentPlayer', gameObj.curPlayer);
				return gameObj.player[gameObj.curPlayer].socket.emit('yourTurn', tmp, gameObj.playCardNumber);
			});
		});
		
	}
	
	//Check if player should end turn
	function checkNextPlayer(win){
		console.log("Inside checkNextPlayer win : " + win);
		console.log("Inside checkNextPlayer win type : " + typeof(win));
		//changed from (win && win != -1 && win != "") to (win != -1) to test win - ching
		var someoneWin = 0;
		//Send hands number to client
		var playerHandsNum = [];
		for(var i = 0; i < gameObj.playerNum; i++){
			playerHandsNum.push(gameObj.player[i].hands.length); 
		}
		gameObj.socket.emit('playerHandsNum', playerHandsNum);
		if(gameObj.winningCondition.length > 1 && typeof(win)!="undefined"){
			console.log("[checkWin][85] For card 85");
			console.log("win[0]", win[0]);
			console.log("win[1]", win[1]);
			if (win[0] != -1 && typeof(win[0]) != "undefined" && win[0] != "" && win[0] >= 0 && win[0] <= 6){
				console.log("[checkWin]Player[" + win[0] + "] wins the game");
				gameObj.socket.emit("win", win[0]);
				someoneWin = 1;
			}
			else if(win[1] != -1 && typeof(win[1]) != "undefined" && win[1] != "" && win[1] >= 0 && win[1] <= 6){
				console.log("[checkWin]Player[" + win[1] + "] wins the game");
				gameObj.socket.emit("win", win[1]);
				someoneWin = 1;
			}
		}
		else if(win != -1 && typeof(win) != "undefined" && win != "" && win >= 0 && win <= 6){
			console.log("Player[" + win + "] wins the game");
			//Send to client side
			gameObj.socket.emit("win", win);
			someoneWin = 1;
		}
		console.log("someoneWin : " + someoneWin);
		if(gameObj.playthelastcard != -1 && someoneWin == 0){		//Player will need to play until X cards remain
			if(gameObj.player[gameObj.curPlayer].hands.length != gameObj.playthelastcard && gameObj.player[gameObj.curPlayer].hands.length > gameObj.playthelastcard){		//Still have more hands
				console.log("Ask current player to playCard number " + gameObj.curPlayerplayed);
				gameObj.player[gameObj.curPlayer].socket.emit('playCard', gameObj.player[gameObj.curPlayer].hands.length - gameObj.playthelastcard);
			}
			else{
				//Next Player
				toNextPlayer();
			}
		}
		else if (someoneWin == 0){
			console.log("current_played: " + gameObj.curPlayerplayed);
			console.log("playcardnumber: " + gameObj.playCardNumber);
			if(gameObj.curPlayerplayed >= gameObj.playCardNumber || gameObj.player[gameObj.curPlayer].hands.length == 0){		
				//Next Player
				toNextPlayer();
			}
			else{
				console.log("Ask current player to playCard number " + gameObj.curPlayerplayed);
				gameObj.player[gameObj.curPlayer].socket.emit('playCard', gameObj.playCardNumber - gameObj.curPlayerplayed);
			}
		}
	}
	
	function checkWin(win){
		var deferred = q.defer();
		console.log("[checkWin] win : " + win);
		console.log("[checkWin] type of win : " + typeof(win));
		if(gameObj.winningCondition.length > 1){
			console.log("[checkWin] For card 85");
			if (win[0] != -1 && typeof(win[0]) != "undefined" && win[0] != "" && win[0] >= 0 && win[0] <= 6){
				console.log("[checkWin]Player[" + win[0] + "] wins the game");
				gameObj.socket.emit("win", win[0]);
			}
			else if(win[1] != -1 && typeof(win[1]) != "undefined" && win[1] != "" && win[1] >= 0 && win[1] <= 6){
				console.log("[checkWin]Player[" + win[1] + "] wins the game");
				gameObj.socket.emit("win", win[1]);
			}
			else {
				deferred.resolve();
				return deferred.promise;
			}
		}
		else if(win != -1 && typeof(win) != "undefined" && win != "" && win >= 0 && win <= 6){
			console.log("[checkWin]Player[" + win + "] wins the game");
			gameObj.socket.emit("win", win);
		}
		else{
			console.log("[checkWin] Returns promise");
			deferred.resolve();
			return deferred.promise;
		}
		
	}
	
	
	io.on( 'connection', function( socket ) {
		console.log( 'New user connected' );
		var room_socket;
		socket.on('register', function(roomID, playerID, callback){
			
			if(playerID == -1){
				socket.join(roomID);
				if(roomIDs.indexOf(roomID) > -1){
					console.log('Another player join Room : ' + roomID);
					callback(-1);
				}
				else{
					console.log('Open Room : ' + roomID);
					//room_socket = socket;
					room_socket = io.to(roomID);
					roomIDs.push(roomID);
				}
			}
			else{
				console.log('User join room : ' + roomID);
			
				if(!players[roomID]){
					players[roomID] = [{"ID" : playerID,"socket" : socket}];
				}
				else{
					players[roomID].push({"ID" : playerID,"socket" : socket});
				}
				
				players[roomID].sort(function (a,b){ 
					if(a.ID > b.ID){
						return 1;
					}
					if (a.ID < b.ID){
						return -1;
					}
					return 0;
				});
				for(var i = 0; i < players[roomID].length; i ++){
					console.log(players[roomID][i].ID);
				}
				
				if(!num_ready[roomID]){
					num_ready[roomID] = 1;
				} 
				else 
					num_ready[roomID]++; 
				
				console.log("Current Number Player : " + num_ready[roomID]);
				
				console.log("player " + playerID + " join room " + roomID);
				io.to(roomID).emit('playerReady', playerID);
				
				if (num_ready[roomID] == 2){
					io.to(roomID).emit('canStart');
				}		
			}
		
		});
	
			
		socket.on('start', function(roomID, callback){
			console.log("Game Start on RoomID : " + roomID);
			gameObj = new game(players[roomID].length);
			card = initCard(gameObj);
			
			//Init Variable
			gameObj.playerNum = players[roomID].length;
			console.log("players[roomID].length: " + players[roomID].length);
			gameObj.socket = room_socket;

			//Put socket into playerObject 
			for(var i = 0; i < gameObj.playerNum; i++){
				console.log("Player ID : " + players[roomID][i].ID);
				gameObj.player[i].socket = players[roomID][i].socket;
				for(var j = 0; j < 3; j++){
					var tmpCard = gameObj.drawCard()
					gameObj.player[i].socket.emit('drawCard', tmpCard);
					gameObj.player[i].hands.push(tmpCard);
				}
			}
			

			//Indicate user that is their turn
			var tmp = [];
			tmp.push(gameObj.drawCard());
			console.log("Plaer Draw Card" + tmp);
			console.log("Sending yourTurn message to player 0");
			gameObj.player[gameObj.curPlayer].hands = gameObj.player[gameObj.curPlayer].hands.concat(tmp);
			gameObj.socket.emit('roomCurrentPlayer', gameObj.curPlayer);
			gameObj.player[0].socket.emit('yourTurn', tmp, 1);		//Problem
			callback(gameObj.playerNum);
			gameObj.socket.emit('start', gameObj.playerNum);
			//Send hands number to client
			var playerHandsNum = [];
			for(var i = 0; i < gameObj.playerNum; i++){
				playerHandsNum.push(gameObj.player[i].hands.length); 
			}
			gameObj.socket.emit('playerHandsNum', playerHandsNum);
		});
		
		socket.on('playCard', function(roomID, cardID){ 
			cardID = parseInt(cardID);
			gameObj.curPlayerplayed++;
			console.log("Number of Card curPlayer has played : " + gameObj.curPlayerplayed + " Played cardID : " + cardID);
			console.log("gameObj.player[gameObj.curPlayer].hands.indexOf(cardID) : " + gameObj.player[gameObj.curPlayer].hands.indexOf(cardID));
			gameObj.socket.emit("roomPlayCard", cardID);
			//Remove From player hands
			gameObj.player[gameObj.curPlayer].hands.splice(gameObj.player[gameObj.curPlayer].hands.indexOf(cardID), 1);
			//Send hands number to client
			var playerHandsNum = [];
			for(var i = 0; i < gameObj.playerNum; i++){
				playerHandsNum.push(gameObj.player[i].hands.length); 
			}
			gameObj.socket.emit('playerHandsNum', playerHandsNum);
			 
			//Assume all card will execute once except belongings
			if(card[cardID].type == 1){			//Action
				
				gameObj.discard.push(cardID);
				//Update Room
				gameObj.socket.emit("discard", gameObj.discard);
				card[cardID].func(checkNextPlayer);
				
				//Check if someone wins
				var calls = [];
				for(var i = 0; i < gameObj.winningCondition.length; i ++){
					//card[gameObj.winningCondition[i]].func(checkWin);
					calls.push(card[gameObj.winningCondition[i]].func());
				}
//				q.all(calls).then(checkWin).done(); 
				q.all(calls).then(checkWin); 
				
			}
			else if(card[cardID].type == 2){	//belongings
				//Update Room
				gameObj.player[gameObj.curPlayer].belongings.push(parseInt(cardID));
				console.log("Adding belongings : " + gameObj.player[gameObj.curPlayer].belongings + " to main page");
				gameObj.socket.emit('belongings', gameObj.curPlayer,gameObj.player[gameObj.curPlayer].belongings);
				
				//Check if someone wins
				var calls = [];
				for(var i = 0; i < gameObj.winningCondition.length; i ++){
					calls.push(card[gameObj.winningCondition[i]].func());
				}
				q.all(calls).then(checkNextPlayer);
			}
			else if (card[cardID].type == 3){	//WinningCondition
				//If 雙重議程
				if(gameObj.rules.indexOf(85) > -1 ){
					//If more are already two winningConditions - discard one
					if(gameObj.winningCondition.length > 1){
						gameObj.player[gameObj.curPlayer].socket.emit("selectCard", "[WinningCondition Limit]Discard a winningCondition", gameObj.winningCondition, function(removeCardID){
							console.log("[85]Current player select discard winningCondition : " + removeCardID);	
							
							//Remove old card 
							gameObj.winningCondition.splice(gameObj.winningCondition.indexOf(removeCardID), 1);
							gameObj.discard.push(removeCardID);
							gameObj.socket.emit('discard', gameObj.discard);
					
							//Add New card
							gameObj.winningCondition.push(cardID);
							
							//Update Room
							console.log("[In Playcard 85]Adding winningCondition : " + gameObj.winningCondition + " to main page");
							gameObj.socket.emit('winningCondition', gameObj.winningCondition);
							
							card[cardID].func().then(checkNextPlayer);
						});
					}
					else{
						gameObj.winningCondition.push(cardID);
						
						//Update Room
						console.log("[In Playcard 85]Adding winningCondition : " + gameObj.winningCondition + " to main page");
						gameObj.socket.emit('winningCondition', gameObj.winningCondition);
						
						card[cardID].func().then(checkNextPlayer);
					} 
				}
				else{
					if(gameObj.winningCondition.length > 0){
						gameObj.discard.push(gameObj.winningCondition[0]);
						gameObj.socket.emit('discard', gameObj.discard);
					}
					var tmp = [];
					tmp.push(cardID);
					gameObj.winningCondition = tmp;
					
					//Update Room
					console.log("Adding winningCondition : " + gameObj.winningCondition + " to main page");
					gameObj.socket.emit('winningCondition', gameObj.winningCondition);
					
					card[cardID].func().then(checkNextPlayer);
				}
			}
			else if (card[cardID].type == 4){	//Rules
				gameObj.rules.push(cardID);
				
				//Update Room
				console.log("Adding rules : " + gameObj.rules + " to main page");
				gameObj.socket.emit('rules', gameObj.rules); 

				card[cardID].func().then(checkNextPlayer);
			}
			
		});
		
			//Test Function to test card function
		socket.on('TestFunc', function(roomID){
			var gameObj = new game(2);
			var card = initCard(gameObj);
			gameObj.player[0].socket = players[roomID][0].socket;

			gameObj.player[0].hands = [1, 3, 5, 4];

			gameObj.player[1].hands = [11, 13, 15];

//			gameObj.player[2].hands = [21, 23, 25];
			gameObj.player[0].belongings = [1, 3, 5, 10];
			gameObj.player[1].belongings = [11, 13, 50, 79];
//			gameObj.player[2].belongings = [21, 23, 25];

			card[8].func();
			

			//gameObj.discard = [9, 19, 29];

			card[87].func();

		});

	});
	return io;

}