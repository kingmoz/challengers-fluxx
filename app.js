var express = require('express'),
	uuid = require('node-uuid'),
	http = require('http'),
	game = require('./api/game'),
	initCard = require('./api/initCard').initCard;

var app = express();
var server_port = process.env.OPENSHIFT_NODEJS_PORT || 8000;
var server_ip_address = process.env.OPENSHIFT_NODEJS_IP || '0.0.0.0';

var server = app.listen( server_port, server_ip_address, function () {
	var host = server.address().address;
	var port = server.address().port;
	console.log( 'Listening at http://%s:%s', host, port );
});
var io = require('./api/socket')(server);

app.use('/css', express.static('css'));
app.use('/fonts', express.static('fonts'));
app.use('/images', express.static('images'));
app.use('/js', express.static('js'));
app.use('/cards', express.static('cards'));
app.use('/user_icons', express.static('user_icons'));

app.get('/', function (req, res) {
	var newRoomID = uuid.v1();
	res.redirect('/' + newRoomID);
});

app.get('/flow', function (req, res) {
	var newRoomID = uuid.v1();
	res.redirect('/flow/' + newRoomID);
});

app.get('/flow/:roomID', function(req, res){
	res.sendFile(__dirname +'/views/index.html');
});

app.get('/flow/:roomID/:playerID', function(req, res){
	res.sendFile(__dirname +'/views/player.html');
});

app.get('/:roomID', function(req, res){
	res.sendFile(__dirname +'/test_index.html');
});

app.get('/:roomID/:playerID', function(req, res){
	res.sendFile(__dirname +'/test_player.html');
});


